import os

from setuptools import find_packages, setup

here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, "README.md")) as f:
    long_description = f.read()

requires = [
    "elasticsearch",
    "SPARQLWrapper",
    "elasticsearch_dsl>=7.0.0,<8.0.0",
    "ijson",
    "requests",
    "click",
]

tests_require = [
    line.strip()
    for line in open(os.path.join(here, "requirements-test.txt"))
    if not line.startswith("#")
]

setup(
    name="ecoplots-indexer",
    version="1.0.3",
    author="Javier Sanchez Gonzalez",
    author_email="j.sanchezgonzalez@uq.edu.au",
    url="https://bitbucket.org/terndatateam/ecoplots-indexer",
    license="LICENSE",
    keywords="",
    use_scm_version={
        # put a version file into module on build to simplify pkg version discovery
        "write_to": "src/indexer/version.py",
        "fallback_version": "0.0.0.dev0",
    },
    setup_requires=["setuptools_scm"],
    description="TERN EcoPlots Indexer.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages("src"),
    package_dir={"": "src"},
    include_package_data=True,
    zip_safe=False,
    extras_require={
        "testing": tests_require,
    },
    install_requires=requires,
    classifiers=[
        "Programming Language :: Python :: 3.7",
        "Operating System :: OS Independent",
    ],
    entry_points={
        "console_scripts": ["ecoplots-indexer=indexer.main:cli"],
    },
)
