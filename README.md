# Ecoplots Indexer

## Getting Started

## Prerequisites


## Running the tests


## Deployment

## Environment variables

## Design


## Authors

* *Javier Sanchez Gonzalez* -  [TERN](https://bitbucket.org/terndatateam)

## License

This project is licensed under the Content in this publication is licensed under Creative Commons
Attribution 4.0 International Licence, available at http://creativecommons.org/licenses/by/4.0/

docker-compose run --entrypoint "ecoimage-packager package --dest-root='bioimage_package_test' --archive-root='/data/Q0055/bioimages-test/archive' --airflow-dagrun-id='test' --package-id=5b427e902acd11eb890dacde48001122" --name ecoimages-container-name-packager --rm packager
