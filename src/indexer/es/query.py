visit_all = """
OPTIONAL {
    ?site_id tern:hasSiteVisit ?site_visit_id .
    ?site_visit_id void:inDataset ?dataset .
    ?site_visit_id dcterms:identifier ?site_visit_id_label .
    ?site_visit_id a ?visit_class .
    ?site_visit_id prov:startedAtTime ?visit_start_date .
    OPTIONAL { ?site_visit_id prov:endedAtTime ?visit_end_date }
    OPTIONAL { ?site_visit_id tern:locationDescription ?site_visit_location_description }
    OPTIONAL { ?site_visit_id tern:siteDescription ?site_visit_description }
}
"""

visit_tern_surveillance = """
?site_id tern:hasSiteVisit ?site_visit_id .
?site_visit_id <urn:ecoplots:published> true .
?site_visit_id void:inDataset ?dataset .
?site_visit_id dcterms:identifier ?site_visit_id_label .
?site_visit_id a ?visit_class .
?site_visit_id prov:startedAtTime ?visit_start_date .
OPTIONAL { ?site_visit_id prov:endedAtTime ?visit_end_date }
OPTIONAL { ?site_visit_id tern:locationDescription ?site_visit_location_description }
OPTIONAL { ?site_visit_id tern:siteDescription ?site_visit_description }
"""


def get_query(
    query: str,
    dataset: str,
    foi: str = None,
    parameter: str = None,
    plot_type: str = None,
    pagination: str = "",
) -> str:
    if query == "observations":
        return f"""
            PREFIX void: <http://rdfs.org/ns/void#>
            PREFIX prov: <http://www.w3.org/ns/prov#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dwct: <http://rs.tdwg.org/dwc/terms/>
            PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
            PREFIX tern-loc: <https://w3id.org/tern/ontologies/loc/>

            SELECT DISTINCT *
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id dcterms:type tern:Observation .
            ?id a ?observation_class .
            ?id tern:hasSite ?site_id .
            ?site_id tern:featureType ?site_type .
            OPTIONAL {{
                ?site_id sosa:isSampleOf* ?top_parent_site_id .
                ?top_parent_site_id tern:featureType <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c>
            }}
            ?id tern:hasSiteVisit ?site_visit_id .
            { "?site_visit_id <urn:ecoplots:published> true ." if dataset == "tern-surveillance" else "" }
            ?site_visit_id prov:startedAtTime ?site_visit_date .
            ?id sosa:hasFeatureOfInterest ?feature_id .
            ?feature_id dcterms:type ?feature_class .
            OPTIONAL {{ ?feature_id sosa:isSampleOf ?parent_feature_id . }}
            # OPTIONAL {{ ?feature_id dwct:materialSampleID ?material_sample_id }}
            ?feature_id tern:featureType ?feature_type .
            VALUES ?feature_type {{
                <{foi}>
            }}
            # OPTIONAL {{
            #     ?feature_id sosa:isResultOf ?sampling_id .
            #     ?sampling_id sosa:usedProcedure ?sampling_method .
            # }}
            ?id sosa:observedProperty ?observed_property .
            VALUES ?observed_property {{
                <{parameter}>
            }}
            ?id sosa:usedProcedure ?used_procedure .
            ?used_procedure skos:prefLabel ?used_procedure_label .
            ?id sosa:hasResult ?_result .
            ?_result rdf:value ?result_value .
            OPTIONAL {{ ?result_value skos:prefLabel ?result_value_label_1 }}
            OPTIONAL {{ ?result_value rdfs:label ?result_value_label_2 }}
            OPTIONAL {{ ?_result tern:unit ?unit_of_measure . }}
            OPTIONAL {{
                ?id sosa:madeBySensor ?sensor_id .
                ?sensor_id dcterms:type ?system_class .
                ?sensor_id tern:systemType ?system_type .
                # ?system_type skos:prefLabel ?system_type_label .
            }}
            ?id tern:resultDateTime ?result_time .
            ?id void:inDataset ?dataset .
            OPTIONAL {{ ?id <urn:ecoplots:tags> ?tags }}
            }}
            {pagination}
            """
    if query == "observations_taxon":
        return f"""
            PREFIX void: <http://rdfs.org/ns/void#>
            PREFIX prov: <http://www.w3.org/ns/prov#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dwct: <http://rs.tdwg.org/dwc/terms/>
            PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
            PREFIX tern-loc: <https://w3id.org/tern/ontologies/loc/>

            SELECT DISTINCT *
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id dcterms:type tern:Observation .
            ?id a ?observation_class .
            ?id tern:hasSite ?site_id .
            ?site_id tern:featureType ?site_type .
            OPTIONAL {{
                ?site_id sosa:isSampleOf* ?top_parent_site_id .
                ?top_parent_site_id tern:featureType <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c>
            }}
            ?id tern:hasSiteVisit ?site_visit_id .
            { "?site_visit_id <urn:ecoplots:published> true ." if dataset == "tern-surveillance" else "" }
            ?site_visit_id prov:startedAtTime ?site_visit_date .
            ?id sosa:hasFeatureOfInterest ?feature_id .
            ?feature_id dcterms:type ?feature_class .
            OPTIONAL {{ ?feature_id sosa:isSampleOf ?parent_feature_id . }}
            # OPTIONAL {{ ?feature_id dwct:materialSampleID ?material_sample_id }}
            ?feature_id tern:featureType ?feature_type .
            VALUES ?feature_type {{
                <{foi}>
            }}
            # OPTIONAL {{
            #     ?feature_id sosa:isResultOf ?sampling_id .
            #     ?sampling_id sosa:usedProcedure ?sampling_method .
            # }}
            ?id sosa:observedProperty ?observed_property .
            ?id sosa:usedProcedure ?used_procedure .
            ?used_procedure skos:prefLabel ?used_procedure_label .
            ?id sosa:hasResult ?_result .
            ?_result dwct:taxonID ?taxon_taxon_id .
            OPTIONAL {{
                ?id sosa:madeBySensor ?sensor_id .
                ?sensor_id dcterms:type ?system_class .
                ?sensor_id tern:systemType ?system_type .
                # ?system_type skos:prefLabel ?system_type_label .
            }}
            ?id tern:resultDateTime ?result_time .
            ?id void:inDataset ?dataset .
            OPTIONAL {{ ?id <urn:ecoplots:tags> ?tags }}
            }}
            {pagination}
            """
    if query == "observations_occur":
        return f"""
            PREFIX void: <http://rdfs.org/ns/void#>
            PREFIX prov: <http://www.w3.org/ns/prov#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dwct: <http://rs.tdwg.org/dwc/terms/>
            PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
            PREFIX tern-loc: <https://w3id.org/tern/ontologies/loc/>

            SELECT DISTINCT *
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id dcterms:type tern:Observation .
            FILTER NOT EXISTS {{ ?id tern:hasSite ?no }}
            ?id a ?observation_class .
            ?id sosa:hasFeatureOfInterest ?feature_id .
            ?id geosparql:hasGeometry ?_geometry .
            ?_geometry geosparql:asWKT ?wkt_point .
            ?_geometry geo:lat ?latitude .
            ?_geometry geo:long ?longitude .
            OPTIONAL {{ ?_geometry geo:alt ?altitude }}
            OPTIONAL {{ ?_geometry tern-loc:elevation ?elevation }}
            ?feature_id dcterms:type ?feature_class .
            OPTIONAL {{ ?feature_id sosa:isSampleOf ?parent_feature_id . }}
            # OPTIONAL {{ ?feature_id dwct:materialSampleID ?material_sample_id }}
            ?feature_id tern:featureType ?feature_type .
            VALUES ?feature_type {{
                <{foi}>
            }}
            # OPTIONAL {{
            #     ?feature_id sosa:isResultOf ?sampling_id .
            #     ?sampling_id sosa:usedProcedure ?sampling_method .
            # }}
            ?id sosa:observedProperty ?observed_property .
            VALUES ?observed_property {{
                <{parameter}>
            }}
            ?id sosa:usedProcedure ?used_procedure .
            ?used_procedure skos:prefLabel ?used_procedure_label .
            ?id sosa:hasResult ?_result .
            ?_result rdf:value ?result_value .
            OPTIONAL {{ ?result_value skos:prefLabel ?result_value_label_1 }}
            OPTIONAL {{ ?result_value rdfs:label ?result_value_label_2 }}

            OPTIONAL {{ ?_result tern:unit ?unit_of_measure . }}
            OPTIONAL {{
                ?id sosa:madeBySensor ?sensor_id .
                ?sensor_id dcterms:type ?system_class .
                ?sensor_id tern:systemType ?system_type .
                # ?system_type skos:prefLabel ?system_type_label .
            }}
            ?id tern:resultDateTime ?result_time .
            ?id void:inDataset ?dataset .
            OPTIONAL {{ ?id <urn:ecoplots:tags> ?tags }}
            }}
            {pagination}
            """
    if query == "observations_occur_taxon":
        return f"""
            PREFIX void: <http://rdfs.org/ns/void#>
            PREFIX prov: <http://www.w3.org/ns/prov#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dwct: <http://rs.tdwg.org/dwc/terms/>
            PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
            PREFIX tern-loc: <https://w3id.org/tern/ontologies/loc/>

            SELECT DISTINCT *
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id dcterms:type tern:Observation .
            FILTER NOT EXISTS {{ ?id tern:hasSite ?no }}
            ?id a ?observation_class .
            ?id sosa:hasFeatureOfInterest ?feature_id .
            ?id geosparql:hasGeometry ?_geometry .
            ?_geometry geosparql:asWKT ?wkt_point .
            ?_geometry geo:lat ?latitude .
            ?_geometry geo:long ?longitude .
            OPTIONAL {{ ?_geometry geo:alt ?altitude }}
            OPTIONAL {{ ?_geometry tern-loc:elevation ?elevation }}
            ?feature_id dcterms:type ?feature_class .
            OPTIONAL {{ ?feature_id sosa:isSampleOf ?parent_feature_id . }}
            # OPTIONAL {{ ?feature_id dwct:materialSampleID ?material_sample_id }}
            ?feature_id tern:featureType ?feature_type .
            VALUES ?feature_type {{
                <{foi}>
            }}
            # OPTIONAL {{
            #     ?feature_id sosa:isResultOf ?sampling_id .
            #     ?sampling_id sosa:usedProcedure ?sampling_method .
            # }}
            ?id sosa:observedProperty ?observed_property .
            ?id sosa:usedProcedure ?used_procedure .
            ?used_procedure skos:prefLabel ?used_procedure_label .
            ?id sosa:hasResult ?_result .
            ?_result dwct:taxonID ?taxon_taxon_id .
            OPTIONAL {{
                ?id sosa:madeBySensor ?sensor_id .
                ?sensor_id dcterms:type ?system_class .
                ?sensor_id tern:systemType ?system_type .
                # ?system_type skos:prefLabel ?system_type_label .
            }}
            ?id tern:resultDateTime ?result_time .
            ?id void:inDataset ?dataset .
            OPTIONAL {{ ?id <urn:ecoplots:tags> ?tags }}
            }}
            {pagination}
            """
    if query == "dataset_attributes":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?id
            ?dataset_id
            ?attribute
            ?value
            ?value_label_1
            ?value_label_2
            ?unit_of_measure
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id a tern:Attribute .
            ?id tern:isAttributeOf ?dataset_id .
            ?dataset_id dcterms:type tern:RDFDataset .
            ?id tern:attribute ?attribute .
            ?id tern:hasValue/rdf:value ?value .
            OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
            OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
            OPTIONAL {{
                ?id tern:hasValue/tern:unit ?unit_of_measure .
            }}
            }}
            """
    if query == "foi_attributes":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
                ?id
                ?feature_id
                ?attribute
                ?value
                ?value_label_1
                ?value_label_2
                ?unit_of_measure
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            {{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?feature_id .
                ?feature_id dcterms:type tern:FeatureOfInterest .
                ?feature_id tern:featureType ?feature_type .
                VALUES ?feature_type {{
                    <{foi}>
                }}
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{
                    ?id tern:hasValue/tern:unit ?unit_of_measure .
                }}
            }}
            UNION
            {{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?feature_id .
                ?feature_id dcterms:type tern:Sample .
                ?feature_id tern:featureType ?feature_type .
                VALUES ?feature_type {{
                    <{foi}>
                }}
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{
                    ?id tern:hasValue/tern:unit ?unit_of_measure .
                }}
            }}
            UNION
            {{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?feature_id .
                ?feature_id dcterms:type tern:MaterialSample .
                ?feature_id tern:featureType ?feature_type .
                VALUES ?feature_type {{
                    <{foi}>
                }}
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{
                    ?id tern:hasValue/tern:unit ?unit_of_measure .
                }}
            }}
            }}
            """
    if query == "obs_attributes":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>

            SELECT DISTINCT
            ?id
            ?observation_id
            ?attribute
            ?value
            ?value_label_1
            ?value_label_2
            ?unit_of_measure
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id a tern:Attribute .
            ?id tern:isAttributeOf ?observation_id .
            ?observation_id dcterms:type tern:Observation .
            ?observation_id sosa:hasFeatureOfInterest ?feature_id .
            ?feature_id tern:featureType ?feature_type .
            VALUES ?feature_type {{
                <{foi}>
            }}
            ?id tern:attribute ?attribute .
            ?id tern:hasValue/rdf:value ?value .
            OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
            OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
            OPTIONAL {{
                ?id tern:hasValue/tern:unit ?unit_of_measure .
            }}
            }}
            """
    if query == "system_attributes":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
                ?id
                ?system_id
                ?attribute
                ?value
                ?value_label_1
                ?value_label_2
                ?unit_of_measure
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            {{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?system_id .
                ?system_id a tern:System .
                ?observation_id sosa:madeBySensor ?system_id .
                ?observation_id sosa:hasFeatureOfInterest ?feature_id .
                ?feature_id tern:featureType ?feature_type .
                VALUES ?feature_type {{
                    <{foi}>
                }}
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{
                ?id tern:hasValue/tern:unit ?unit_of_measure .
                }}
            }}
            UNION
            {{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?system_id .
                ?system_id a tern:Sampler .
                ?sampling_id sosa:madeSampling ?system_id .
                ?sampling_id sosa:hasResult ?sample_id .
                ?sample_id sosa:hasFeatureOfInterest ?feature_id .
                ?feature_id tern:featureType ?feature_type .
                VALUES ?feature_type {{
                    <{foi}>
                }}
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{
                ?id tern:hasValue/tern:unit ?unit_of_measure .
                }}
            }}
            UNION
            {{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?system_id .
                ?system_id a tern:Sensor .
                ?observation_id sosa:madeBySensor ?system_id .
                ?observation_id sosa:hasFeatureOfInterest ?feature_id .
                ?feature_id tern:featureType ?feature_type .
                VALUES ?feature_type {{
                    <{foi}>
                }}
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{
                ?id tern:hasValue/tern:unit ?unit_of_measure .
                }}
            }}
            }}
            """
    if query == "sampling":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dwct: <http://rs.tdwg.org/dwc/terms/>

            SELECT DISTINCT 
                ?feature_id
                ?sampling_id
                ?sampling_method
                ?material_sample_id
                ?system_id
                ?system_class
                ?system_type
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id dcterms:type tern:Observation .
            ?id sosa:hasFeatureOfInterest ?feature_id .
            ?feature_id tern:featureType ?feature_type .
            VALUES ?feature_type {{
                <{foi}>
            }}
            ?feature_id sosa:isResultOf ?sampling_id .
            ?sampling_id a tern:Sampling .
            ?sampling_id sosa:usedProcedure ?sampling_method .
            OPTIONAL {{ ?feature_id dwct:materialSampleID ?material_sample_id }}
            OPTIONAL {{
                ?sampling_id sosa:madeBySampler ?system_id .
                ?system_id dcterms:type ?system_class .
                ?system_id tern:systemType ?system_type .
            }}
            }}
            """
    if query == "sampling_attributes":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?id
            ?sampling_id
            ?attribute
            ?value
            ?value_label_1
            ?value_label_2
            ?unit_of_measure
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id a tern:Attribute .
            ?id tern:isAttributeOf ?sampling_id .
            ?sampling_id a tern:Sampling .
            ?feature_id sosa:isResultOf ?sampling_id .
            ?feature_id tern:featureType ?feature_type .
            VALUES ?feature_type {{
                <{foi}>
            }}
            ?id tern:attribute ?attribute .
            ?id tern:hasValue/rdf:value ?value .
            OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
            OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
            OPTIONAL {{
                ?id tern:hasValue/tern:unit ?unit_of_measure .
            }}
            }}
            """
    if query == "site_attributes":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
                ?id
                ?site_id
                ?attribute
                ?value
                ?value_label_1
                ?value_label_2
                ?unit_of_measure
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            {{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?site_id .
                ?site_id tern:featureType <http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2> . # plot
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{ ?id tern:hasValue/tern:unit ?unit_of_measure }}
            }}
			UNION
			{{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?site_id .
                ?site_id tern:featureType <http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4> . # site
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{ ?id tern:hasValue/tern:unit ?unit_of_measure }}
            }}
			UNION
			{{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?site_id .
                ?site_id tern:featureType <http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1> . # transect
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{ ?id tern:hasValue/tern:unit ?unit_of_measure }}
            }}
			UNION
			{{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?site_id .
                ?site_id tern:featureType <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c> . # parent site
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{ ?id tern:hasValue/tern:unit ?unit_of_measure }}
            }}
			UNION
			{{
                ?id a tern:Attribute .
                ?id tern:isAttributeOf ?site_id .
                ?site_id tern:featureType <http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5> . # quadrat                
                ?id tern:attribute ?attribute .
                ?id tern:hasValue/rdf:value ?value .
                OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
                OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
                OPTIONAL {{ ?id tern:hasValue/tern:unit ?unit_of_measure }}
            }}
			}}
            """
    if query == "site_visit_attributes":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

            SELECT DISTINCT
            ?id
            ?site_visit_id
            ?attribute
            ?value
            ?value_label_1
            ?value_label_2
            ?unit_of_measure
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id a tern:Attribute .
            ?id tern:isAttributeOf ?site_visit_id .
            ?site_visit_id a tern:SiteVisit .
            ?id tern:attribute ?attribute .
            ?id tern:hasValue/rdf:value ?value .
            OPTIONAL {{ ?value skos:prefLabel ?value_label_1 }}
            OPTIONAL {{ ?value rdfs:label ?value_label_2 }}
            OPTIONAL {{
                ?id tern:hasValue/tern:unit ?unit_of_measure .
            }}
            }}
            """
    if query == "datasets":
        return f"""
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX void: <http://rdfs.org/ns/void#>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX schema: <http://schema.org/>

            SELECT *
            FROM <http://{dataset}>
            # FROM <http://vocabs>
            WHERE {{
                ?dataset_uri a tern:RDFDataset .
                ?dataset_uri a ?dataset_class .
                ?dataset_uri dcterms:title ?dataset_label .
                OPTIONAL {{ ?dataset_uri dcterms:alternative ?dataset_alt_label . }}
                OPTIONAL {{ ?dataset_uri dcterms:description ?dataset_description . }}
                OPTIONAL {{ ?dataset_uri dcterms:abstract ?dataset_abstract . }}
                OPTIONAL {{ ?dataset_uri dcterms:issued ?dataset_version . }}
                OPTIONAL {{ ?dataset_uri dcterms:created ?dataset_created . }}
                OPTIONAL {{ 
                    ?dataset_uri dcterms:publisher ?dataset_publisher . 
                    ?dataset_publisher schema:name ?dataset_publisher_label . 
                }}
                OPTIONAL {{ ?dataset_uri dcterms:bibliographicCitation ?dataset_citation . }}
                OPTIONAL {{ ?dataset_uri void:subset ?dataset_parent . }}
                OPTIONAL {{ ?dataset_uri <urn:ecoplots:dataset_type> ?dataset_type . }}
            }}
            """
    if query == "dataset_citation":
        return f"""
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>

            SELECT *
            FROM <http://{dataset}>
            WHERE {{
                ?dataset_uri a tern:RDFDataset .
                ?dataset_uri dcterms:bibliographicCitation ?dataset_citation .
            }}
            """
    if query == "dataset_authors":
        return f"""
            PREFIX schema: <http://schema.org/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>

            SELECT ?dataset_uri ?type ?order ?author ?name
            FROM <http://{dataset}>
            WHERE {{
                {{
                    BIND("author" AS ?type)
                    #?dataset_uri a tern:RDFDataset .
                    ?dataset_uri dcterms:creator ?dataset_authors .
                    ?dataset_authors ?order ?author .
                    ?author schema:name ?name .
                }}
                UNION
                {{
                    BIND("coAuthor" AS ?type)
                    ?dataset_uri tern:coAuthor ?dataset_coauthors .
                    ?dataset_coauthors ?order ?author .
                    ?author schema:name ?name .
                }}
                UNION
                {{
                    BIND("publisher" AS ?type)
                    ?dataset_uri dcterms:publisher ?author .
                    BIND(<http://www.w3.org/1999/02/22-rdf-syntax-ns#_1> AS ?order)
                    ?author schema:name ?name .
                }}
            }}
            """
    if query == "dataset_metadata":
        return f"""
            PREFIX dcat: <http://www.w3.org/ns/dcat#>

            SELECT *
            FROM <http://{dataset}>
            WHERE {{
                ?dataset_uri dcat:landingPage ?dataset_metadata .
            }}
            """
    if query == "dataset_project":
        return f"""
            PREFIX void: <http://rdfs.org/ns/void#>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>

            SELECT DISTINCT *
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?dataset_uri a tern:RDFDataset .
                ?dataset_uri dcterms:title ?dataset_label .
                OPTIONAL {{
                ?dataset_uri void:subset ?dataset_parent .
                ?dataset_parent dcterms:title ?dataset_parent_label .
                }}
            }}
            """
    if query == "ufoi_sites":
        return f"""
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

            SELECT DISTINCT
            ?id
            ?ufoi
            FROM <http://{dataset}>
            WHERE {{
                ?id tern:featureType ?site_type .
                values ?site_type {{
                <http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2> # plot
                <http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4> # site
                <http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1> # transect
                <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c> # parent site
                <http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5> # quadrat
                }}
                ?id geosparql:sfWithin ?ufoi .
            }}
            """
    if query == "ufoi_obs":
        return f"""
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

            SELECT DISTINCT
            ?id
            ?ufoi
            FROM <http://{dataset}>
            WHERE {{
                ?obs_id dcterms:type tern:Observation.
                ?obs_id geosparql:sfWithin ?ufoi .
                ?obs_id geosparql:hasGeometry ?_geometry .
                ?_geometry geosparql:asWKT ?id .            
            }}
            {pagination}
            """
    if query == "visit_count":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>

            SELECT DISTINCT ?site_id (COUNT(?site_visit_id) AS ?site_visit_count)
            FROM <http://{dataset}>
            WHERE {{
            ?site_visit_id a tern:SiteVisit .
            ?site_visit_id tern:hasSite ?site_id .
            ?site_id tern:featureType <http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4> #site
            }}
            GROUP BY ?site_id
            """
    if query == "site_and_location":
        return f"""
            PREFIX dct: <http://purl.org/dc/terms/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX so: <http://schema.org/>
            PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
            PREFIX void: <http://rdfs.org/ns/void#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX prov: <http://www.w3.org/ns/prov#>
            PREFIX tern-loc: <https://w3id.org/tern/ontologies/loc/>

            SELECT DISTINCT
                ?site_class
                ?visit_class
                ?site_id
                ?site_id_label
                ?site_visit_id
                ?site_visit_id_label
                ?visit_start_date
                ?visit_end_date
                ?site_visit_location_description
                ?site_visit_description
                ?dataset
                ?date_commissioned
                ?dimension
                ?location_description
                ?site_description
                ?feature_type
                ?wkt_polygon
                ?wkt_point
                ?point_type
                ?latitude
                ?longitude
                ?altitude
                ?elevation
                ?transect_direction
                ?parent_site_id
                ?parent_site_label
                ?top_parent_site_id
                ?top_parent_site_label
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?site_id dcterms:type ?site_class .
                ?site_id dcterms:identifier ?site_id_label .
                BIND (<{plot_type}> AS ?feature_type) .
                ?site_id tern:featureType ?feature_type .                
                ?site_id void:inDataset ?dataset .
                { visit_tern_surveillance if dataset == "tern-surveillance" else visit_all }
                OPTIONAL {{ ?site_id tern:dateCommissioned ?date_commissioned }}
                OPTIONAL {{ ?site_id tern:dimension ?dimension }}
                OPTIONAL {{ ?site_id tern:locationDescription ?location_description }}
                OPTIONAL {{ ?site_id tern:siteDescription ?site_description }}
                OPTIONAL {{
                    ?site_id geosparql:hasGeometry ?_geometry_polygon .
                    ?_geometry_polygon a tern-loc:Polygon .
                    ?_geometry_polygon geosparql:asWKT ?wkt_polygon .
                }}
                OPTIONAL {{
                    ?site_id geosparql:hasGeometry ?_geometry_point .
                    ?_geometry_point a tern-loc:Point .
                    ?_geometry_point geosparql:asWKT ?wkt_point .
                    ?_geometry_point tern-loc:pointType ?point_type .
                    ?_geometry_point geo:lat ?latitude .
                    ?_geometry_point geo:long ?longitude .
                    OPTIONAL {{ ?_geometry_point geo:alt ?altitude }}
                    OPTIONAL {{ ?_geometry_point tern-loc:elevation ?elevation }}
                }}
                OPTIONAL {{ ?site_id tern:transectDirection ?transect_direction }}
                OPTIONAL {{
                    ?site_id sosa:isSampleOf ?parent_site_id .
                    ?parent_site_id dcterms:type ?parent_site_class .
                    VALUES (?parent_site_class) {{
                        (tern:Site) (tern:Transect)
                    }}
                    OPTIONAL {{ ?parent_site_id dcterms:identifier ?parent_site_label . }}
                }}
                OPTIONAL {{
                    ?site_id sosa:isSampleOf* ?top_parent_site_id .
                    ?top_parent_site_id tern:featureType <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c> .
                    OPTIONAL {{ ?top_parent_site_id dcterms:identifier ?top_parent_site_label . }}
                }}
            }}
            """
    if query == "site_tags":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?site_id
            ?tags
            FROM <http://{dataset}>
            WHERE {{
            {{
                ?site_id tern:featureType ?site_type .
                values ?site_type {{
                <http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2> # plot
                <http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4> # site
                <http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1> # transect
                <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c> # parent site
                <http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5> # quadrat
                }}
                ?obs_id tern:hasSite ?site_id .
                ?obs_id <urn:ecoplots:tags> ?tags .
            }}
            UNION {{
                ?transect_id dcterms:type ?site_class
                values (?site_class) {{
                (tern:Transect)
                }}
                ?obs_id tern:hasSite ?transect_id .
                ?obs_id <urn:ecoplots:tags> ?tags .
                ?transect_id sosa:isSampleOf ?site_id .
            }}
            }}
            """
    if query == "attr_details":
        return f"""
            PREFIX void: <http://rdfs.org/ns/void#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

            SELECT DISTINCT
                ?attribute_uri
                ?dataset
                ?value_type
                ?attribute_label
                ?attribute_definition
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?id a tern:Attribute .
                ?id tern:attribute ?attribute_uri.
                ?id void:inDataset ?dataset .
                ?id tern:hasValue ?_value .
                ?_value a ?value_type .
                FILTER(?value_type IN (tern:IRI)) .
                # FILTER(?value_type NOT IN (tern:Value)) .
                ?attribute_uri skos:prefLabel ?attribute_label .
                OPTIONAL {{ ?attribute_uri skos:definition ?attribute_definition . }}
            }}
            """
    if query == "param_details":
        return f"""
            PREFIX void: <http://rdfs.org/ns/void#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>

            SELECT DISTINCT
                ?observed_property_uri
                ?dataset
                ?value_type  
                ?observed_property_label
                ?observed_property_definition
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?id a tern:Observation.
                ?id sosa:observedProperty ?observed_property_uri .
                ?id void:inDataset ?dataset .
                ?id sosa:hasResult ?_value .
                ?_value a ?value_type .
                FILTER(?value_type IN (tern:IRI)) .
                # FILTER(?value_type NOT IN (tern:Value)) .
                ?observed_property_uri skos:prefLabel ?observed_property_label .
                OPTIONAL {{ ?observed_property_uri  skos:definition ?observed_property_definition . }}
            }}
            """
    if query == "sites_tree":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?site_id
            ?site_type
            ?site_id_label
            ?parent_site_id
            ?parent_site_id_label
            ?parent_site_type
            FROM <http://{dataset}>
            WHERE {{
            ?site_id tern:featureType ?site_type .
            values ?site_type {{
                <http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2> # plot
                <http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4> # site
                <http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1> # transect
                <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c> # parent site
                <http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5> # quadrat
            }}
            ?site_id dcterms:identifier ?site_id_label .
            OPTIONAL {{
                ?site_id sosa:isSampleOf  ?parent_site_id .
                ?parent_site_id tern:featureType ?parent_site_type .
                values ?parent_site_type {{
                <http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2> # plot
                <http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4> # site
                <http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1> # transect
                <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c> # parent site
                <http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5> # quadrat
                }}
                ?parent_site_id dcterms:identifier ?parent_site_id_label .
            }}
            }}
            """
    if query == "sites_label":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?site_id
            ?site_id_label
            FROM <http://{dataset}>
            WHERE {{
            ?site_id tern:featureType ?site_type .
            values ?site_type {{
                <http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2> # plot
                <http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4> # site
                <http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1> # transect
                <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c> # parent site
                <http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5> # quadrat
            }}
            ?site_id dcterms:identifier ?site_id_label .
            }}
            """
    if query == "site_visit_tree":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>

            SELECT DISTINCT
            ?site_visit_id
            ?parent_site_visit_id
            FROM <http://{dataset}>
            WHERE {{
            ?site_visit_id a tern:SiteVisit
            OPTIONAL {{?site_visit_id sosa:isSampleOf ?parent_site_visit_id }}
            }}
            """
    if query == "site_visit_label ":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?site_visit_id
            ?site_visit_id_label
            FROM <http://{dataset}>
            WHERE {{
            ?site_visit_id a tern:SiteVisit .
            ?site_visit_id dcterms:identifier ?site_visit_id_label .
            }}
            """
    if query == "fois":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

            SELECT DISTINCT ?feature_type_key ?feature_type
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?feature_id tern:featureType ?feature_type .
                ?feature_type skos:inScheme <http://linked.data.gov.au/def/tern-cv/68af3d25-c801-4089-afff-cf701e2bd61d> .
                ?feature_type skos:notation ?feature_type_key .
                #VALUES ?feature_type {{
                    # # 1. Landform
                    # <http://linked.data.gov.au/def/tern-cv/2cf3ed29-440e-4a50-9bbc-5aab30df9fcd>
                    # #2. Plant occurrence
                    #<http://linked.data.gov.au/def/tern-cv/b311c0d3-4a1a-4932-a39c-f5cdc1afa611>
                    # # 3. Climate
                    #<http://linked.data.gov.au/def/tern-cv/6d40d71e-58cd-4f75-8304-40c01fe5f74c>
                    # # 4. Land surface
                    # <http://linked.data.gov.au/def/tern-cv/8282fb22-4135-415c-8ca2-317860d102fb>
                    # # 5. Land surface substrate
                    # <http://linked.data.gov.au/def/tern-cv/aef12cd6-3826-4988-a54c-8578d3fb4c8d>
                    # # 6. Plant community
                    #<http://linked.data.gov.au/def/tern-cv/ea3a4c64-dac3-4660-809a-8ad5ced8997b>
                    # # 7. Plant specimen
                    # <http://linked.data.gov.au/def/tern-cv/2e122e23-881c-43fa-a921-a8745f016ceb>
                    # # Soil
                    # <http://linked.data.gov.au/def/tern-cv/98e8d72d-f361-41ed-af9d-6e7f90c1dfce>
                    # # 8. Soil profile
                    # <http://linked.data.gov.au/def/tern-cv/80c39b95-0912-4267-bb66-2fa081683723>
                    # # 9. Soil sample
                    # <http://linked.data.gov.au/def/tern-cv/06461021-a6c2-4175-9651-23653c2b9116>
                    # # 10. Vegetation stratum
                    # <http://linked.data.gov.au/def/tern-cv/32834f36-a478-45be-97f4-ff2ff51e9f5c>
                    # # 11. Animal occurrence
                    # <http://linked.data.gov.au/def/tern-cv/2361dea8-598c-4b6f-a641-2b98ff199e9e>
                    # # 12. Land surface disturbance
                    # <http://linked.data.gov.au/def/tern-cv/7e256d28-e686-4b6a-b64a-ac1b1a8f164d>
                    # # 13. Vegetation disturbance
                    # <http://linked.data.gov.au/def/tern-cv/d4fc54b1-0ad3-4512-86b7-d42b121ece45>
                    # # 14. Geologic substrate
                    # <http://linked.data.gov.au/def/tern-cv/afc81cca-9122-4e36-823d-31dd765e9257>
                    # # 15. Plant individual
                    # <http://linked.data.gov.au/def/tern-cv/60d7edf8-98c6-43e9-841c-e176c334d270>
                    # # 16. Fungal occurrence
                    # <http://linked.data.gov.au/def/tern-cv/45a73139-f6bf-47b7-88d4-4b2865755545>
                    # # 17. Plant population
                    # <http://linked.data.gov.au/def/tern-cv/ae71c3f6-d430-400f-a1d4-97a333b4ee02>
                    # # 18. taxa per stratum
                    # <http://linked.data.gov.au/def/tern-cv/972669a7-8a3c-4bd6-9d24-04606fcdd773>
                    # # 19. Individual stem
                    # <http://linked.data.gov.au/def/tern-cv/caab05f4-084e-4cd8-b219-199529807d3d>
                    # # 20. Plant functional type
                    # <http://linked.data.gov.au/def/tern-cv/c8a66c8a-af06-44fc-b556-3ff242e622aa>
                    # # 21. Ant population
                    # <http://linked.data.gov.au/def/tern-cv/f6e064c6-e2e9-4f9a-90dd-66434a93d985>
                    # # 22. Mammal occurrence
                    # #<http://linked.data.gov.au/def/tern-cv/e16f8309-5db3-4efb-ba4d-a352fb3dcd94>
                    # # 23. Mammal individual
                    # <http://linked.data.gov.au/def/tern-cv/226a127b-7248-476a-84ad-0412a5e3af52>
                    # # 24. Bird occurrence
                    # <http://linked.data.gov.au/def/tern-cv/0c30e871-63ae-4f84-a37c-ef6e7ce02928>
                    # # 25. Fish occurrence
                    # <http://linked.data.gov.au/def/tern-cv/51e0e6c3-b833-4f32-8d0e-00cc76031b83>
                    # # 26. Reptile occurrence
                    # <http://linked.data.gov.au/def/tern-cv/c4e95889-9205-432f-9748-e83966e742c9>
                    # # 27. Reptile individual
                    # <http://linked.data.gov.au/def/tern-cv/d63f3330-5687-4c55-bb91-184fa062a755>
                    # # 28. Fish individual
                    # <http://linked.data.gov.au/def/tern-cv/5ba6ba64-cf48-4c1e-93e5-3892c2b92f4b>
                    # # 29. Amphibian occurrence
                    # #<http://linked.data.gov.au/def/tern-cv/415be87e-66e1-4a53-a5c8-3d34ab93036c>
                    # # 30. Amphibian individual
                    # <http://linked.data.gov.au/def/tern-cv/47fbe947-4f8b-4586-9cf7-13a610f8aedb>
                    # # 31. Invertebrate occurrence
                    # <http://linked.data.gov.au/def/tern-cv/bc02be3f-6c9c-4abe-bec9-5c2de8497897>
                    # # 32. Invertebrate individual
                    # <http://linked.data.gov.au/def/tern-cv/b123913b-d601-4777-9298-7576c44879d2>
                    # # stand vegetation fuel
                    # <http://linked.data.gov.au/def/tern-cv/edebcd9a-a42e-49ba-a629-056cb1a7ba73>
                    # # quadrat vegetation fuel sample
                    # <http://linked.data.gov.au/def/tern-cv/e590824a-ab58-4ea4-9026-47f337273f45>
                    # # plant litter
                    # <http://linked.data.gov.au/def/tern-cv/e6ed6e58-5916-4d31-9ed5-109ab3436fce>
                    # # dead wood
                    # <http://linked.data.gov.au/def/tern-cv/a8b1a7fb-b479-48ff-b706-b3cc4691710b>
                    # # stand dead wood
                    # <http://linked.data.gov.au/def/tern-cv/0e354d29-e23f-4ba7-84d6-9b58ca289782>
                    # # stand plant litter
                    # <http://linked.data.gov.au/def/tern-cv/699b7fcb-6816-4f2a-9f2f-72b8a913f1d0>
                    # # stand plant matter
                    # <http://linked.data.gov.au/def/tern-cv/a0bcca19-143c-4898-9f43-321bad63ebad>
                    # # plant branch individual
                    # <http://linked.data.gov.au/def/tern-cv/07a75262-f9b7-458d-87ee-28614f535da6>
                    # weather
                    #<http://linked.data.gov.au/def/tern-cv/e196c39e-959f-4dd4-8816-d0a58b7cc630>
                #}}
            }}
            """
    if query == "parameters_foi":
        return f"""
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>

            SELECT ?observed_property ?type COUNT(?id) as ?count
            FROM <http://{dataset}>
            WHERE {{
                {{
                    ?id sosa:hasFeatureOfInterest ?feature_id .
                    ?id tern:hasSite ?site_id .
                    ?feature_id tern:featureType ?feature_type .
                    VALUES ?feature_type {{
                        <{foi}>
                    }}
                    ?id sosa:observedProperty ?observed_property .
                    # VALUES ?observed_property {{
                    # <http://linked.data.gov.au/def/tern-cv/70646576-6dc7-4bc5-a9d8-c4c366850df0>
                    # }}
                    BIND("su" AS ?type) 
                }}
                UNION
                {{
                    ?id sosa:hasFeatureOfInterest ?feature_id .
                    FILTER NOT EXISTS {{ ?id tern:hasSite ?no }}
                    ?feature_id tern:featureType ?feature_type .
                    VALUES ?feature_type {{
                        <{foi}>
                    }}
                    ?id sosa:observedProperty ?observed_property .
                    BIND("op" AS ?type)  
                }}
            }} GROUP BY ?observed_property ?type
            """
    if query == "datasets_labels":
        return f"""
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>

            SELECT DISTINCT ?uri ?label
            FROM <http://{dataset}>
            WHERE {{
                ?uri a tern:RDFDataset .
                ?uri dcterms:title ?label .
            }}
            """
    if query == "units_labels":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

            SELECT DISTINCT ?uri ?label
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?_result tern:unit ?uri .
                ?uri rdfs:label ?label .
                FILTER (lang(?label) = 'en')
            }}
            """
    if query == "regions_labels":
        return f"""
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

            SELECT DISTINCT ?uri ?label
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?site_id geosparql:sfWithin ?uri .
            {{ ?uri skos:prefLabel ?label }}
            UNION
            {{ ?uri rdfs:label ?label }}
            }}
            """
    if query == "region_type_labels":
        return f"""
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>

            SELECT DISTINCT ?uri ?label
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id geosparql:sfWithin ?ufoi .
            BIND(
                if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/asgs2016/.+'), <http://linked.data.gov.au/dataset/asgs2016/stateorterritory>,
                if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/capad-2018-terrestrial/.+'), <http://linked.data.gov.au/dataset/capad-2018-terrestrial>,
                    if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/wwf-terr-ecoregions/.+'), <http://linked.data.gov.au/dataset/wwf-terr-ecoregions>,
                    if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/local-gov-areas-2011/.+'), <http://linked.data.gov.au/dataset/local-gov-areas-2011>,
                        if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/nrm-2017/.+'), <http://linked.data.gov.au/dataset/nrm-2017>,
                        if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/bioregion/[a-zA-Z]+[0-9]+'), <http://linked.data.gov.au/dataset/bioregion>,
                            if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/bioregion/[a-zA-Z]+$'), <http://linked.data.gov.au/dataset/bioregion/IBRA7>, "unknown")
                        )
                        )
                    )
                    )
                )
                ) as ?uri
            )
            BIND(
                if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/asgs2016/.+'), "States and territories",
                if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/capad-2018-terrestrial/.+'), "Terrestrial CAPAD regions",
                    if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/wwf-terr-ecoregions/.+'), "WWF ecoregions",
                    if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/local-gov-areas-2011/.+'), "Local government areas",
                        if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/nrm-2017/.+'), "NRM regions",
                        if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/bioregion/[a-zA-Z]+[0-9]+'), "Subregions",
                            if(regex(str(?ufoi), 'http://linked.data.gov.au/dataset/bioregion/[a-zA-Z]+$'), "Bioregions", "unknown")
                        )
                        )
                    )
                    )
                )
                ) as ?label
            )
            }}
            """
    if query == "feature_type_labels":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

            SELECT DISTINCT ?uri ?label ?key
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id dcterms:type tern:Observation .
            ?id sosa:hasFeatureOfInterest ?feature_id .
            ?feature_id tern:featureType ?uri .
            ?uri skos:prefLabel ?label .
            ?uri skos:notation ?key .
            }}
            """
    if query == "observed_property_labels":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

            SELECT DISTINCT ?uri ?label
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id dcterms:type tern:Observation .
            ?id sosa:observedProperty ?uri .
            ?uri skos:prefLabel ?label .
            }}
            """
    if query == "attributes_labels":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

            SELECT DISTINCT
            ?uri
            ?label
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id a tern:Attribute .
            ?id tern:attribute ?uri.
            ?uri skos:prefLabel ?label .
            }}
            """
    if query == "site_id_labels":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?uri
            ?label
            FROM <http://{dataset}>
            WHERE {{
            ?uri tern:featureType ?site_type .
            values ?site_type {{
                <http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2> # plot
                <http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4> # site
                <http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1> # transect
                <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c> # parent site
                <http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5> # quadrat
            }}
            ?uri dcterms:identifier ?label .
            }}
            """
    if query == "site_id_with_data":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?uri
            FROM <http://{dataset}>
            WHERE {{
            ?uri tern:featureType ?site_type .
            values ?site_type {{
                <http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2> # plot
                <http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4> # site
                <http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1> # transect
                <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c> # parent site
                <http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5> # quadrat
            }}
            ?obs_id tern:hasSite ?uri .
            ?obs_id dcterms:type tern:Observation .
            }}
            """
    if query == "sites_fts":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?site_id
            ?site_id_label
            ?site_description
            ?site_location_description
            FROM <http://{dataset}>
            WHERE {{
            ?site_id tern:featureType ?site_type .
            values ?site_type {{
                <http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2> # plot
                <http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4> # site
                <http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1> # transect
                <http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c> # parent site
                <http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5> # quadrat
            }}
            ?site_id dcterms:identifier ?site_id_label .
            OPTIONAL {{ ?site_id tern:locationDescription ?site_location_description }}
            OPTIONAL {{ ?site_id tern:siteDescription ?site_description }}
            }}
            """
    if query == "site_visit_id_labels":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?uri
            ?label
            FROM <http://{dataset}>
            WHERE {{
            ?uri a tern:SiteVisit .
            ?uri dcterms:identifier ?label .
            }}
            """
    if query == "visits_fts":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT DISTINCT
            ?visit_id
            ?visit_id_label
            ?visit_description
            ?visit_location_description
            FROM <http://{dataset}>
            WHERE {{
            ?visit_id a tern:SiteVisit .
            ?visit_id dcterms:identifier ?visit_id_label .
            OPTIONAL {{ ?visit_id tern:locationDescription ?visit_location_description }}
            OPTIONAL {{ ?visit_id tern:siteDescription ?visit_description }}
            }}
            """
    if query == "methods_labels":
        return f"""
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

            SELECT DISTINCT
            ?uri
            ?label
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?id sosa:usedProcedure ?uri .
            ?uri skos:prefLabel ?label .
            }}
            """
    if query == "system_type_labels":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

            SELECT DISTINCT
            ?uri
            ?label
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
            ?system_id tern:systemType ?uri .
            ?uri skos:prefLabel ?label .
            }}
            """
    if query == "core_attributes_labels":
        return f"""
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

            SELECT DISTINCT
            ?uri
            ?label
            FROM <http://vocabs>
            WHERE {{
            {{
                ?id a skos:ConceptScheme .
                VALUES ?id {{
                <http://linked.data.gov.au/def/tern-cv/74aa68d3-28fd-468d-8ff5-7e791d9f7159>
                <http://linked.data.gov.au/def/tern-cv/55d56d24-e393-4852-b6ad-537a9942cfe3>
                <http://linked.data.gov.au/def/tern-cv/6ea67eea-c2ae-4240-952a-fdb95b2df8ab>
                }}
                ?uri skos:inScheme ?id .
                ?uri skos:prefLabel ?label .
            }}
            UNION
                {{
                ?id a skos:Collection .
                VALUES ?id {{
                <http://linked.data.gov.au/def/tern-cv/84a3b2fd-0919-4afe-a4a2-7b2c643ebbea>
                }}
                ?id skos:member ?uri .
                ?uri skos:prefLabel ?label .
            }}
            }}
            """
    if query == "site_coordinates":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
            PREFIX tern-loc: <https://w3id.org/tern/ontologies/loc/>

            SELECT DISTINCT
                ?site_id
                ?wkt_point
                ?latitude
                ?longitude
                ?altitude
                ?elevation
            FROM <http://{dataset}>
            WHERE {{
                ?site_id dcterms:type tern:Site .
                ?site_id geosparql:hasGeometry ?_geometry .
                ?_geometry geosparql:asWKT ?wkt_point .
                ?_geometry geo:lat ?latitude .
                ?_geometry geo:long ?longitude .
                OPTIONAL {{ ?_geometry geo:alt ?altitude }}
                OPTIONAL {{ ?_geometry tern-loc:elevation ?elevation }}
            }}
            """
    if query == "scientific_foi":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX dwct: <http://rs.tdwg.org/dwc/terms/>

            SELECT DISTINCT
            ?feature_id
            ?species_name
            FROM <http://{dataset}>
            WHERE {{
            ?id dcterms:type tern:Observation .
            ?id sosa:hasFeatureOfInterest ?feature_id .
            ?feature_id tern:featureType ?feature_type .
            VALUES ?feature_type {{
                <{foi}>
            }}
            ?id sosa:observedProperty <http://linked.data.gov.au/def/tern-cv/56195246-ec5d-4050-a1c6-af786fbec715> . #scientific_name
            ?id sosa:hasResult ?_result .
            ?_result rdf:value ?species_name .
            }}
            """
    if query == "accepted_foi":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX dwct: <http://rs.tdwg.org/dwc/terms/>

            SELECT DISTINCT
                ?feature_id
                ?species_name
                ?classification
            FROM <http://{dataset}>
            WHERE {{
                ?id dcterms:type tern:Observation .
                ?id sosa:hasFeatureOfInterest ?feature_id .
                ?feature_id tern:featureType ?feature_type .
                VALUES ?feature_type {{
                    <{foi}>
                }}
                ?id sosa:observedProperty <http://linked.data.gov.au/def/tern-cv/70646576-6dc7-4bc5-a9d8-c4c366850df0> . #taxon
                ?id sosa:hasResult ?_result .
                ?_result dwct:acceptedNameUsage ?species_name .
                OPTIONAL {{ ?_result dwct:higherClassification ?classification }}
            }}
        """
    if query == "species_by_site":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX dwct: <http://rs.tdwg.org/dwc/terms/>

            SELECT ?site_id (GROUP_CONCAT(DISTINCT ?species_name; SEPARATOR="|") AS ?species_list)
            FROM <http://{dataset}>
            WHERE {{
                ?id dcterms:type tern:Observation .
                ?id tern:hasSite ?site_id .                
                {{ 
                    ?id sosa:observedProperty <http://linked.data.gov.au/def/tern-cv/56195246-ec5d-4050-a1c6-af786fbec715> .
                    ?id sosa:hasResult ?_result .
                    ?_result rdf:value ?species_name . 
                }} 
                UNION 
                {{ 
                    ?id sosa:observedProperty <http://linked.data.gov.au/def/tern-cv/70646576-6dc7-4bc5-a9d8-c4c366850df0> .
                    ?id sosa:hasResult ?_result .
                    ?_result dwct:acceptedNameUsage ?species_name . 
                }}
            }} 
            GROUP BY ?site_id
        """
    if query == "classification_by_site":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX dwct: <http://rs.tdwg.org/dwc/terms/>

            SELECT ?site_id (GROUP_CONCAT(DISTINCT ?classification; SEPARATOR="|") AS ?classification_list)
            FROM <http://{dataset}>
            WHERE {{
                ?id dcterms:type tern:Observation .
                ?id tern:hasSite ?site_id .
                ?id sosa:observedProperty <http://linked.data.gov.au/def/tern-cv/70646576-6dc7-4bc5-a9d8-c4c366850df0> .
                ?id sosa:hasResult ?_result .
                ?_result dwct:higherClassification ?classification
            }}
            GROUP BY ?site_id
        """
    if query == "fois_by_site":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT ?site_id (GROUP_CONCAT(DISTINCT ?feature_type; SEPARATOR="|") AS ?feature_type_list)
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?id dcterms:type tern:Observation .
                ?id tern:hasSite ?site_id .
                ?id sosa:hasFeatureOfInterest ?feature_id .
                ?feature_id tern:featureType ?feature_type .
            }}
            GROUP BY ?site_id
        """
    if query == "parameters_by_site":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT ?site_id (GROUP_CONCAT(DISTINCT ?parameter; SEPARATOR="|") AS ?parameter_list)
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?id dcterms:type tern:Observation .
                ?id tern:hasSite ?site_id .
                ?id sosa:observedProperty ?parameter .
            }}
            GROUP BY ?site_id
        """
    if query == "parameters_by_foi_by_site":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT ?site_id ?feature_type
                (GROUP_CONCAT(DISTINCT ?parameter; SEPARATOR="|") AS ?parameters_by_foi_list)
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?observation dcterms:type tern:Observation .
                ?observation tern:hasSite ?site_id .
                ?observation sosa:hasFeatureOfInterest ?feature_id .
                ?feature_id tern:featureType ?feature_type .
                ?observation sosa:observedProperty ?parameter .
            }}
            GROUP BY ?site_id ?feature_type
        """
    if query == "procedures_by_site":
        return f"""
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>

            SELECT ?site_id (GROUP_CONCAT(DISTINCT ?used_procedure; SEPARATOR="|") AS ?procedure_list)
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?id dcterms:type tern:Observation .
                ?id tern:hasSite ?site_id .
                ?id sosa:usedProcedure ?used_procedure .
            }}
            GROUP BY ?site_id
        """
    if query == "taxon":
        return f"""
            PREFIX void: <http://rdfs.org/ns/void#>
            PREFIX prov: <http://www.w3.org/ns/prov#>
            PREFIX tern: <https://w3id.org/tern/ontologies/tern/>
            PREFIX sosa: <http://www.w3.org/ns/sosa/>
            PREFIX dcterms: <http://purl.org/dc/terms/>
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX dwct: <http://rs.tdwg.org/dwc/terms/>
            PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
            PREFIX geosparql: <http://www.opengis.net/ont/geosparql#>
            PREFIX tern-loc: <https://w3id.org/tern/ontologies/loc/>

            SELECT DISTINCT 
                ?taxon_label
                ?taxon_taxon_id
                ?taxon_accepted_name_label
                ?taxon_accepted_name 
                ?taxon_higher_classification
                ?taxon_rank 
                ?taxon_class
                ?taxon_family
                ?taxon_genus
                ?taxon_kingdom 
                ?taxon_order
                ?taxon_phylum
                ?taxon_vernacular_name 
                ?taxon_name_according_to_label 
                ?taxon_name_according_to
                ?taxon_nomenclatural_code
                ?taxon_nomenclatural_status
                ?taxon_scientific_name_label 
                ?taxon_scientific_name_authorship 
                ?taxon_scientific_name 
                ?taxon_concept_id
                ?taxon_taxonomic_status 
            FROM <http://{dataset}>
            FROM <http://vocabs>
            WHERE {{
                ?id dcterms:type tern:Observation .
                ?id sosa:hasResult ?_result .
                ?_result rdfs:label ?taxon_label .
                ?_result dwct:taxonID ?taxon_taxon_id .
                OPTIONAL {{ ?_result dwct:acceptedNameUsage ?taxon_accepted_name_label }}
                OPTIONAL {{ ?_result dwct:acceptedNameUsageID ?taxon_accepted_name }}
                OPTIONAL {{ ?_result dwct:higherClassification ?taxon_higher_classification }}
                OPTIONAL {{ ?_result dwct:taxonRank ?taxon_rank }}
                OPTIONAL {{ ?_result dwct:class ?taxon_class }}
                OPTIONAL {{ ?_result dwct:family ?taxon_family }}
                OPTIONAL {{ ?_result dwct:genus ?taxon_genus }}
                OPTIONAL {{ ?_result dwct:kingdom ?taxon_kingdom }}
                OPTIONAL {{ ?_result dwct:order ?taxon_order }}
                OPTIONAL {{ ?_result dwct:phylum ?taxon_phylum }}
                OPTIONAL {{ ?_result dwct:vernacularName ?taxon_vernacular_name }}
                OPTIONAL {{ ?_result dwct:nameAccordingTo ?taxon_name_according_to_label }}
                OPTIONAL {{ ?_result dwct:nameAccordingToID ?taxon_name_according_to }}
                OPTIONAL {{ ?_result dwct:nomenclaturalCode ?taxon_nomenclatural_code }}
                OPTIONAL {{ ?_result dwct:nomenclaturalStatus ?taxon_nomenclatural_status }}
                OPTIONAL {{ ?_result dwct:scientificName ?taxon_scientific_name_label }}
                OPTIONAL {{ ?_result dwct:scientificNameAuthorship ?taxon_scientific_name_authorship }}
                OPTIONAL {{ ?_result dwct:scientificNameID ?taxon_scientific_name }}
                OPTIONAL {{ ?_result dwct:taxonConceptID ?taxon_concept_id }}
                OPTIONAL {{ ?_result dwct:taxonomicStatus ?taxon_taxonomic_status }}
            }}
        """

    raise ValueError("Missing requested query: check for typos or create a new query.")
