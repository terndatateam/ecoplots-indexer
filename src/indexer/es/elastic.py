import logging
import math
import os
import ssl
import time
from typing import Callable
import uuid
from datetime import datetime
from itertools import groupby
from elasticsearch import Elasticsearch, exceptions, helpers
from SPARQLWrapper import JSON, SPARQLWrapper
from concurrent.futures import ThreadPoolExecutor

from indexer.es.mapping import (
    dataset_mapping,
    label_mapping,
    observation_mapping,
    site_mapping,
)
from indexer.es.query import get_query
from indexer.es.generate_index_actions import (
    TERN_CV_PREFIX,
    TERN_CV_SHORT,
    create_dataset_action,
    create_dataset_project_tree,
    create_site_or_visit_fts,
    create_site_or_visit_label,
    create_site_or_visit_tree,
    create_sites_coordinates,
    create_sites_tree,
    flatten_attributes,
    flatten_citations,
    flatten_authors,
    flatten_metadata,
    flatten_creators,
    flatten_site_visit_count,
    flatten_tags,
    flatten_ufoi,
    flatten_sampling,
    process_by_site,
    process_by_foi_by_site,
    generate_obs_occurrence_actions,
    generate_observation_actions,
    generate_labels_actions,
    generate_sites,
    get_list_fois,
    get_list_params,
    get_list_sites_w_data,
    get_species,
    get_species_classification,
    get_value,
    get_taxon,
    process_sites,
)


SPARQL_TIMEOUT = 100000
SPARQL_BATCH = 1000000

MAX_MEMORY_CHUNK_SIZE = 150000000

SITE_VISIT_KEYS = ["site_visit_id", "visit_start_date", "visit_end_date"]

TAXON_URI = "http://linked.data.gov.au/def/tern-cv/70646576-6dc7-4bc5-a9d8-c4c366850df0"

indexing_summary = {}


def get_labels_queries(dataset: str) -> dict:
    return {
        "dataset": get_query("datasets_labels", dataset),
        "region": get_query("regions_labels", dataset),
        "region_type": get_query("region_type_labels", dataset),
        "site_id": get_query("site_id_labels", dataset),
        "site_visit_id": get_query("site_visit_id_labels", dataset),
        "feature_type": get_query("feature_type_labels", dataset),
        "observed_property": get_query("observed_property_labels", dataset),
        "attributes": get_query("attributes_labels", dataset),
        "units": get_query("units_labels", dataset),
        "used_procedure": get_query("methods_labels", dataset),
        "system_type": get_query("system_type_labels", dataset),
        "core_attributes": get_query("core_attributes_labels", dataset),
    }


def get_es_connection(
    es_hosts: str or list, es_user: str, es_pass: str, es_timeout: int = 600
) -> Elasticsearch:
    logger = logging.getLogger(__name__)
    # Build and test connection to Elasticsearch
    es = Elasticsearch(
        hosts=es_hosts,
        http_auth=(es_user, es_pass),
        use_ssl=False,
        verify_certs=False,
        timeout=es_timeout,
    )
    try:
        es.info()
    except Exception as e:
        logger.error("Can't connect to elasticsearch: %s", e)
        raise
    return es


def update_index(es: Elasticsearch, index_actions: set, chunk_size: int = 2000) -> None:
    """Ingest all index_actions into index named index_name.

    After successful indexing update alias to include new index and delete all
    previously existing indices on alias.
    """
    logger = logging.getLogger(__name__)
    try:
        helpers.bulk(es, index_actions, chunk_size=chunk_size)
    except exceptions.ConnectionTimeout as e:
        retry_seconds = 30
        while retry_seconds < 500:
            try:
                helpers.bulk(es, index_actions, chunk_size=chunk_size)
                break
            except exceptions.ConnectionTimeout as e:
                logger.warning(f"ReadTimeoutError... retrying again in {retry_seconds}s")
                time.sleep(retry_seconds)
                retry_seconds *= 2
                continue
    except exceptions.TransportError as e:
        if e.status_code == 413 or e.status_code == 429:
            logger.warning(
                f"Request Entity Too Large (chuck size={chunk_size}). Retrying with smaller size"
            )
            chunck_retry = 1
            while chunck_retry < 5:
                try:
                    helpers.bulk(
                        es,
                        index_actions,
                        chunk_size=int(chunk_size / (2 * chunck_retry)),
                    )
                    break
                except exceptions.TransportError as e:
                    if e.status_code == 413 or e.status_code == 429:
                        logger.warning(
                            f"Request Entity Too Large (chuck size={chunk_size}). Retrying with smaller size"
                        )
                        chunck_retry += 1
                        continue
                    else:
                        logger.error("Indexing failed: %s", e)
                        raise
        else:
            logger.error("Indexing failed: %s", e)
            raise
    except Exception as e:
        logger.error("Indexing failed: %s", e)
        raise


def switch_index_in_alias(
    es: Elasticsearch,
    alias: str,
    dataset: str,
    subindex: str,
    index_name: str,
    subindex2: str = None,
) -> None:
    """Ingest all index_actions into index named index_name.

    After successful indexing update alias to include new index and delete all
    previously existing indices on alias.
    """
    logger = logging.getLogger(__name__)
    start_time = time.time()
    try:
        # 1. get existing indices
        # collect alias update actions
        update_actions = []
        for idx in es.indices.get(f"{alias}*").keys():
            # remove currently existing indices
            if idx != index_name:
                # ensure we don't remove new index
                if f"{dataset}" in idx:
                    # ensure we don't remove indices from other datasets
                    if subindex:
                        if subindex in idx:
                            if subindex2:
                                if f"_{subindex2}_" in idx:
                                    update_actions.append({"remove_index": {"index": idx}})
                            else:
                                update_actions.append({"remove_index": {"index": idx}})
                    else:
                        update_actions.append({"remove_index": {"index": idx}})

        # logger.info('Ingesting documents into Elasticsearch {}'.format(index_name))
        # helpers.bulk(es, index_actions, chunk_size=1000)

        logger.info("Switch alias to new index")
        # add alias to new index
        update_actions.append({"add": {"index": index_name, "alias": alias}})
        # update index aliases
        es.indices.update_aliases({"actions": update_actions})
        logger.info(list(es.indices.get(alias).keys()))
        logger.info(f"Index switched in {time.time() - start_time:.2f} seconds.")

    except Exception as e:
        logger.error("Switch index in alias failed: %s", e)
        # TODO: depending on failure we might want to clean up es indices.
        raise


def index_dataset_information(
    dataset: str,
    sparql_endpoint: str,
    sparql_query: str,
    sparql_username: str,
    sparql_password: str,
    index_alias: str,
    es_hosts: str or list,
    es_user: str,
    es_pass: str,
    dataset_tree: dict,
    attributes_dataset_dict: dict,
    temp_dir: str,
) -> None:
    logger = logging.getLogger(__name__)
    logger.info(f"Updating [{index_alias}] index.")
    start_time = time.time()

    dataset_citations = get_dataset_citations(
        dataset,
        sparql_endpoint,
        sparql_username,
        sparql_password,
        temp_dir,
    )

    dataset_authors, dataset_coauthors, dataset_publishers = get_dataset_authors(
        dataset,
        sparql_endpoint,
        sparql_username,
        sparql_password,
        temp_dir,
    )

    dataset_metadata = get_dataset_metadata(
        dataset,
        sparql_endpoint,
        sparql_username,
        sparql_password,
        temp_dir,
    )

    # Hack to make anzograph sparql queries working
    # TODO: review need of this hack in upcoming versions of Anzograph
    if not os.environ.get("PYTHONHTTPSVERIFY", "") and getattr(
        ssl, "_create_unverified_context", None
    ):
        ssl._create_default_https_context = ssl._create_unverified_context

    sparql = SPARQLWrapper(sparql_endpoint)
    if sparql_username is not None and sparql_password is not None:
        sparql.setCredentials(sparql_username, sparql_password)
    sparql.setQuery(sparql_query)
    sparql.method = "POST"
    sparql.setTimeout(SPARQL_TIMEOUT)
    sparql.setReturnFormat(JSON)
    sparql_response = sparql.query().convert()

    datasets = []
    for row in sparql_response["results"]["bindings"]:
        dataset_uri = get_value("dataset_uri", row)
        publishers = list(dataset_publishers.get(dataset_uri, {}).get("by_dataset", set()))
        if publishers:
            pub_uri = publishers[0][1]
            pub_name = publishers[0][2]
        else:
            pub_uri = None
            pub_name = None
        index_doc = {
            "dataset_ecoplots_type": (
                get_value("dataset_type", row) if get_value("dataset_type", row) else "datasource"
            ),
            "dataset_class": get_value("dataset_class", row),
            "dataset_uri": dataset_uri,
            "dataset_label": get_value("dataset_label", row),
            "dataset_alt_label": get_value("dataset_alt_label", row),
            "dataset_description": get_value("dataset_description", row),
            "dataset_abstract": get_value("dataset_abstract", row),
            "dataset_version": get_value("dataset_version", row),
            "dataset_created": get_value("dataset_created", row),
            "dataset_publisher": pub_uri,
            "dataset_publisher_label": pub_name,
            "dataset_citation": dataset_citations.get(dataset_uri),
            "dataset_author": dataset_authors.get(dataset_uri),
            "dataset_coauthor": dataset_coauthors.get(dataset_uri),
            "dataset_metadata": dataset_metadata.get(dataset_uri),
            "dataset_parent": get_value("dataset_parent", row),
            "dataset_parent_label": (
                dataset_tree[dataset_uri][1] if dataset_tree[dataset_uri] else None
            ),
        }

        new_dataset_attr = set()
        for attr in attributes_dataset_dict.get(dataset_uri, []):
            attr_uri = attr["attribute"]
            new_dataset_attr.add(attr_uri)
            index_doc[f"dataset_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"] = attr
        index_doc["dataset_attributes"] = list(new_dataset_attr)
        datasets.append(index_doc)

    index_name = f"{index_alias}-{dataset}_{datetime.now().strftime('%Y%m%d%H%M%S')}"
    es = get_es_connection(es_hosts, es_user, es_pass)
    es.indices.create(index_name, body=dataset_mapping)
    es.indices.put_settings({"index": {"refresh_interval": "-1"}}, index=index_name)

    for index_actions in create_dataset_action(index_name, datasets):
        update_index(es, index_actions, 500)

    try:
        es.indices.forcemerge(index_name, max_num_segments=1)
        es.indices.refresh(index_name)
        switch_index_in_alias(es, index_alias, dataset, None, index_name)
    except exceptions.ConnectionTimeout as e:
        retry_seconds = 30
        while retry_seconds < 500:
            try:
                es.indices.forcemerge(index_name, max_num_segments=1)
                es.indices.refresh(index_name)
                switch_index_in_alias(es, index_alias, dataset, None, index_name)
                break
            except exceptions.ConnectionTimeout as e:
                logger.warning(f"ReadTimeoutError... retrying again in {retry_seconds}s")
                time.sleep(retry_seconds)
                retry_seconds *= 2
                continue

    logger.info(f"Completed dataset version indexing in {time.time() - start_time:.2f} seconds.")


def index_data(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    index_alias: str,
    es_hosts: str or list,  # type: ignore
    es_user: str,
    es_pass: str,
    temp_dir: str,
    regions: dict,
    attributes_site_dict: dict,
    attributes_site_visit_dict: dict,
    dataset_project_tree: dict,
    attributes_dataset_dict: dict,
    site_visit_count: dict,
    sites_fts_dict: dict,
    sites_tree_dict: dict,
    index_labels: bool = True,
    chunk_size: int = 2000,
    **kwargs,
) -> None:
    """Index the results of a SPARQL query into Elasticsearch.

    Additional kwargs passed in will be made available to the function assigned to prep_index_actions_func.
    """

    sites_location_dict = get_sites_coordinates(
        dataset,
        sparql_endpoint,
        sparql_username,
        sparql_password,
        temp_dir,
    )

    visits_fts_dict = get_site_or_visit_fts(
        "visit",
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("visits_fts", dataset),
        temp_dir,
    )

    fois_dict = get_feature_of_interest(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("fois", dataset),
        temp_dir,
    )

    if index_labels:
        for facet, sparql_query in get_labels_queries(dataset).items():
            ingest_labels(
                dataset,
                es_hosts,
                es_pass,
                es_user,
                facet,
                f"{index_alias}-labels-{facet}",
                sparql_endpoint,
                sparql_username,
                sparql_password,
                sparql_query,
                temp_dir,
            )

    ingest_data(
        dataset,
        es_hosts,
        es_pass,
        es_user,
        f"{index_alias}-data",
        "observations",
        chunk_size,
        sparql_endpoint,
        sparql_username,
        sparql_password,
        temp_dir,
        regions,
        attributes_site_dict,
        attributes_site_visit_dict,
        sites_tree_dict,
        sites_fts_dict,
        visits_fts_dict,
        fois_dict,
        dataset_project_tree,
        attributes_dataset_dict,
        sites_location_dict,
        site_visit_count,
    )


def index_site_data(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    index_alias: str,
    es_hosts: str or list,
    es_user: str,
    es_pass: str,
    temp_dir: str,
    regions: dict,
    attributes_site_dict: dict,
    attributes_site_visit_dict: dict,
    dataset_project_tree: dict,
    attributes_dataset_dict: dict,
    site_visit_count: dict,
    sites_fts_dict: dict,
    sites_tree_dict: dict,
    chunk_size: int = 2000,
    **kwargs,
) -> None:
    """Index the results of a SPARQL query into Elasticsearch.

    Additional kwargs passed in will be made available to the function assigned to prep_index_actions_func.
    """

    ingest_site_data(
        dataset,
        es_hosts,
        es_pass,
        es_user,
        index_alias,
        chunk_size,
        sparql_endpoint,
        sparql_username,
        sparql_password,
        temp_dir,
        regions,
        attributes_site_dict,
        attributes_site_visit_dict,
        dataset_project_tree,
        attributes_dataset_dict,
        site_visit_count,
        sites_fts_dict,
        sites_tree_dict,
    )


def ingest_data(
    dataset,
    es_hosts,
    es_pass,
    es_user,
    index_alias,
    subindex,
    chunk_size,
    sparql_endpoint,
    sparql_username,
    sparql_password,
    temp_dir,
    regions_dict,
    attributes_site_dict,
    attributes_site_visit_dict,
    sites_tree_dict,
    sites_fts_dict,
    visits_fts_dict,
    fois_dict,
    dataset_project_tree,
    attributes_dataset_dict,
    sites_location_dict,
    site_visit_count,
):
    logger = logging.getLogger(__name__)

    start_time = time.time()

    taxon_dict = get_all_taxon(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("taxon", dataset),
        temp_dir,
    )

    for foi_label, foi_uri in fois_dict.items():
        logger.info(f"Starting bulk indexing for '{foi_label}' in ES: {es_hosts}")

        start_time_2 = time.time()
        attributes_foi_dict = get_attributes(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            get_query("foi_attributes", dataset, foi_uri),
            "feature_id",
            temp_dir,
        )

        attributes_obs_dict = get_attributes(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            get_query("obs_attributes", dataset, foi_uri),
            "observation_id",
            temp_dir,
        )

        attributes_system_dict = get_attributes(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            get_query("system_attributes", dataset, foi_uri),
            "system_id",
            temp_dir,
        )

        attributes_sampling_dict = get_attributes(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            get_query("sampling_attributes", dataset, foi_uri),
            "sampling_id",
            temp_dir,
        )

        scientific_name_dict = get_species_name(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            get_query("scientific_foi", dataset, foi_uri),
            temp_dir,
        )

        accepted_name_dict, classification_dict = get_species_and_classification(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            get_query("accepted_foi", dataset, foi_uri),
            temp_dir,
        )

        sampling_dict = get_sampling(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            get_query("sampling", dataset, foi_uri),
            temp_dir,
        )

        index_name = f"{index_alias}-{subindex}-{dataset}_{foi_label}_{datetime.now().strftime('%Y%m%d%H%M%S')}"
        # index_name = (
        #     "plotdata_ecoplots-data-observations-tern-surveillance_plant-occurrence_20240618203939"
        # )
        es = get_es_connection(es_hosts, es_user, es_pass)
        es.indices.create(index_name, body=observation_mapping)
        es.indices.put_settings({"index": {"refresh_interval": "-1"}}, index=index_name)

        summary = {}

        params_dict = get_parameters(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            get_query("parameters_foi", dataset, foi_uri),
            temp_dir,
        )

        for param_uri, param_count in params_dict.items():
            if param_uri == TAXON_URI:
                continue
            # Occurrence data (except Taxon)
            op_count = param_count.get("op", 0)
            if op_count > 0:
                batches = math.ceil(op_count / SPARQL_BATCH)
                for i in range(0, batches):
                    pagination = (
                        f"LIMIT {SPARQL_BATCH} {f'OFFSET {SPARQL_BATCH*i}' if i>0 else ''}"
                        if batches > 1
                        else ""
                    )
                    index_occurrence_obs(
                        sparql_endpoint,
                        sparql_username,
                        sparql_password,
                        get_query(
                            "observations_occur",
                            dataset,
                            foi_uri,
                            param_uri,
                            None,
                            pagination,
                        ),
                        temp_dir,
                        regions_dict,
                        dataset_project_tree,
                        attributes_dataset_dict,
                        foi_label,
                        param_uri,
                        attributes_foi_dict,
                        attributes_obs_dict,
                        attributes_system_dict,
                        attributes_sampling_dict,
                        scientific_name_dict,
                        accepted_name_dict,
                        classification_dict,
                        taxon_dict,
                        sampling_dict,
                        summary,
                        es,
                        index_name,
                        chunk_size,
                    )
                    merge_and_refresh_index(logger, index_name, es)

            # Plot data
            su_count = param_count.get("su", 0)
            if su_count > 0:
                batches = math.ceil(su_count / SPARQL_BATCH)
                for i in range(0, batches):
                    pagination = (
                        f"LIMIT {SPARQL_BATCH} {f'OFFSET {SPARQL_BATCH*i}' if i>0 else ''}"
                        if batches > 1
                        else ""
                    )
                    index_obs(
                        sparql_endpoint,
                        sparql_username,
                        sparql_password,
                        get_query(
                            "observations",
                            dataset,
                            foi_uri,
                            param_uri,
                            None,
                            pagination,
                        ),
                        temp_dir,
                        regions_dict,
                        attributes_site_dict,
                        attributes_site_visit_dict,
                        sites_tree_dict,
                        sites_fts_dict,
                        visits_fts_dict,
                        dataset_project_tree,
                        attributes_dataset_dict,
                        sites_location_dict,
                        site_visit_count,
                        foi_label,
                        param_uri,
                        attributes_foi_dict,
                        attributes_obs_dict,
                        attributes_system_dict,
                        attributes_sampling_dict,
                        scientific_name_dict,
                        accepted_name_dict,
                        classification_dict,
                        taxon_dict,
                        sampling_dict,
                        summary,
                        es,
                        index_name,
                        chunk_size,
                    )
                    merge_and_refresh_index(logger, index_name, es)

        op_count = params_dict.get(TAXON_URI, {}).get("op", 0)
        if op_count > 0:
            batches = math.ceil(op_count / SPARQL_BATCH)
            for i in range(0, batches):
                pagination = (
                    f"LIMIT {SPARQL_BATCH} {f'OFFSET {SPARQL_BATCH*i}' if i>0 else ''}"
                    if batches > 1
                    else ""
                )
                # Occurrence data (Taxon)
                index_occurrence_obs(
                    sparql_endpoint,
                    sparql_username,
                    sparql_password,
                    get_query("observations_occur_taxon", dataset, foi_uri, None, pagination),
                    temp_dir,
                    regions_dict,
                    dataset_project_tree,
                    attributes_dataset_dict,
                    foi_label,
                    "taxon",
                    attributes_foi_dict,
                    attributes_obs_dict,
                    attributes_system_dict,
                    attributes_sampling_dict,
                    scientific_name_dict,
                    accepted_name_dict,
                    classification_dict,
                    taxon_dict,
                    sampling_dict,
                    summary,
                    es,
                    index_name,
                    chunk_size,
                    taxon=True,
                )
                merge_and_refresh_index(logger, index_name, es)

        su_count = params_dict.get(TAXON_URI, {}).get("su", 0)
        if su_count > 0:
            batches = math.ceil(su_count / SPARQL_BATCH)
            for i in range(0, batches):
                pagination = (
                    f"LIMIT {SPARQL_BATCH} {f'OFFSET {SPARQL_BATCH*i}' if i>0 else ''}"
                    if batches > 1
                    else ""
                )
                # Plot data (Taxon)
                index_obs(
                    sparql_endpoint,
                    sparql_username,
                    sparql_password,
                    get_query("observations_taxon", dataset, foi_uri, None, pagination),
                    temp_dir,
                    regions_dict,
                    attributes_site_dict,
                    attributes_site_visit_dict,
                    sites_tree_dict,
                    sites_fts_dict,
                    visits_fts_dict,
                    dataset_project_tree,
                    attributes_dataset_dict,
                    sites_location_dict,
                    site_visit_count,
                    foi_label,
                    "taxon",
                    attributes_foi_dict,
                    attributes_obs_dict,
                    attributes_system_dict,
                    attributes_sampling_dict,
                    scientific_name_dict,
                    accepted_name_dict,
                    classification_dict,
                    taxon_dict,
                    sampling_dict,
                    summary,
                    es,
                    index_name,
                    chunk_size,
                    taxon=True,
                )
                merge_and_refresh_index(logger, index_name, es)

        switch_index_in_alias(es, index_alias, dataset, subindex, index_name, foi_label)

        # try:
        #     es.indices.forcemerge(index_name, max_num_segments=1)
        #     es.indices.refresh(index_name)

        # except exceptions.ConnectionTimeout as e:
        #     retry_seconds = 30
        #     while retry_seconds < 500:
        #         try:
        #             es.indices.forcemerge(index_name, max_num_segments=1)
        #             es.indices.refresh(index_name)
        #             switch_index_in_alias(es, index_alias, dataset, subindex, index_name, foi_label)
        #             break
        #         except exceptions.ConnectionTimeout as e:
        #             logger.warning(f"ReadTimeoutError... retrying again in {retry_seconds}s")
        #             time.sleep(retry_seconds)
        #             retry_seconds *= 2
        #             continue

        summary["total"] = time.time() - start_time_2
        indexing_summary[f"{foi_label}"] = summary

    logger.info(f"Completed indexing in {time.time() - start_time:.2f} seconds.")
    foi_len = len(indexing_summary)
    i = 1
    for foi, summ in indexing_summary.items():
        logger.info(f"|__Indexed '{foi}' in {summ['total']:.2f} seconds.")
        q_len = len(summ) - 1
        q_i = 1
        for q, q_times in summ.items():
            if q != "total":
                logger.info(
                    f"{'|' if i<foi_len else ' '}  |__Indexed '{q}' in {q_times[0]+q_times[1]:.2f} seconds."
                )
                logger.info(
                    f"{'|' if i<foi_len else ' '}  {'|' if q_i<q_len else ' '}  |__Query: {q_times[0]:.2f}s."
                )
                logger.info(
                    f"{'|' if i<foi_len else ' '}  {'|' if q_i<q_len else ' '}  |__Processing + indexing: {q_times[1]:.2f}s."
                )
            q_i += 1
        i += 1


def merge_and_refresh_index(logger, index_name, es):
    try:
        es.indices.forcemerge(index_name, max_num_segments=1)
        es.indices.refresh(index_name)
    except exceptions.ConnectionTimeout as e:
        retry_seconds = 30
        while retry_seconds < 500:
            try:
                es.indices.forcemerge(index_name, max_num_segments=1)
                es.indices.refresh(index_name)
                break
            except exceptions.ConnectionTimeout as e:
                logger.warning(f"ReadTimeoutError... retrying again in {retry_seconds}s")
                time.sleep(retry_seconds)
                retry_seconds *= 2
                continue


def index_obs(
    sparql_endpoint,
    sparql_username,
    sparql_password,
    query,
    temp_dir,
    regions_dict,
    attributes_site_dict,
    attributes_site_visit_dict,
    sites_tree_dict,
    sites_fts_dict,
    visits_fts_dict,
    dataset_project_tree,
    attributes_dataset_dict,
    sites_location_dict,
    site_visit_count,
    foi_label,
    param,
    attributes_foi_dict,
    attributes_obs_dict,
    attributes_system_dict,
    attributes_sampling_dict,
    scientific_name_dict,
    accepted_name_dict,
    classification_dict,
    taxon_dict,
    sampling_dict,
    summary,
    es,
    index_name,
    chunk_size,
    taxon=False,
):
    logger = logging.getLogger(__name__)
    start_time = time.time()
    times = []
    logger.info(
        f"Executing sparql '{foi_label}'-'{param}' {'(taxon) ' if taxon else ''}query against endpoint: {sparql_endpoint}"
    )
    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        query,
        temp_dir,
    )
    logger.info(f"Completed query in {time.time() - start_time:.2f} seconds.")

    times.append(time.time() - start_time)
    start_time = time.time()

    for index_actions in generate_observation_actions(
        index_name,
        temp_filename,
        regions_dict,
        attributes_foi_dict,
        attributes_obs_dict,
        attributes_system_dict,
        attributes_site_dict,
        attributes_site_visit_dict,
        attributes_sampling_dict,
        dataset_project_tree,
        attributes_dataset_dict,
        sites_tree_dict,
        sites_fts_dict,
        visits_fts_dict,
        site_visit_count,
        sites_location_dict,
        scientific_name_dict,
        accepted_name_dict,
        classification_dict,
        taxon_dict,
        sampling_dict,
    ):
        update_index(es, index_actions, chunk_size)

    times.append(time.time() - start_time)
    logger.info(
        f"Completed indexing '{foi_label}' {'(taxon) ' if taxon else ''} in {times[0]+times[1]:.2f} seconds."
    )
    summary["{}{}".format(foi_label, "__taxon" if taxon else "")] = times

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")


def index_occurrence_obs(
    sparql_endpoint,
    sparql_username,
    sparql_password,
    query,
    temp_dir,
    regions_dict,
    dataset_project_tree,
    attributes_dataset_dict,
    foi_label,
    param,
    attributes_foi_dict,
    attributes_obs_dict,
    attributes_system_dict,
    attributes_sampling_dict,
    scientific_name_dict,
    accepted_name_dict,
    classification_dict,
    taxon_dict,
    sampling_dict,
    summary,
    es,
    index_name,
    chunk_size,
    taxon=False,
):
    logger = logging.getLogger(__name__)
    start_time = time.time()
    times = summary.get("{}__occurrence{}".format(foi_label, "__taxon" if taxon else ""), [0, 0])
    logger.info(
        f"Executing sparql '{foi_label}'-'{param}' (occurrence) {'(taxon) ' if taxon else ''}query against endpoint: {sparql_endpoint}"
    )
    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        query,
        temp_dir,
    )
    # logger.info(f"Completed query in {time.time() - start_time:.2f} seconds.")

    times[0] = times[0] + (time.time() - start_time)
    start_time = time.time()

    for index_actions in generate_obs_occurrence_actions(
        index_name,
        temp_filename,
        regions_dict,
        attributes_foi_dict,
        attributes_obs_dict,
        attributes_system_dict,
        attributes_sampling_dict,
        dataset_project_tree,
        attributes_dataset_dict,
        scientific_name_dict,
        accepted_name_dict,
        classification_dict,
        taxon_dict,
        sampling_dict,
    ):
        update_index(es, index_actions, chunk_size)

    times[1] = times[1] + (time.time() - start_time)
    # logger.info(
    #     f"Completed indexing '{foi_label}' (occurrence) {'(taxon) ' if taxon else ''} in {times[0]+times[1]:.2f} seconds."
    # )
    summary["{}__occurrence{}".format(foi_label, "__taxon" if taxon else "")] = times

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")


def fetch_and_process_site(
    sparql_endpoint,
    sparql_username,
    sparql_password,
    dataset,
    temp_dir,
    sites_tree_dict,
):
    def fetch_site_tags():
        return get_site_tags(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            get_query("site_tags", dataset),
            temp_dir,
        )

    def fetch_species_by_site():
        return get_resource_by_site(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            "species_list",
            get_query("species_by_site", dataset),
            process_by_site,
            temp_dir,
        )

    def fetch_classification_by_site():
        return get_resource_by_site(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            "classification_list",
            get_query("classification_by_site", dataset),
            process_by_site,
            temp_dir,
        )

    def fetch_fois_by_site():
        return get_resource_by_site(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            "feature_type_list",
            get_query("fois_by_site", dataset),
            process_by_site,
            temp_dir,
        )

    def fetch_parameters_by_site():
        return get_resource_by_site(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            "parameter_list",
            get_query("parameters_by_site", dataset),
            process_by_site,
            temp_dir,
        )

    def fetch_procedures_by_site():
        return get_resource_by_site(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            "procedure_list",
            get_query("procedures_by_site", dataset),
            process_by_site,
            temp_dir,
        )

    def fetch_parameters_by_foi_by_site():
        return get_resource_by_site(
            sparql_endpoint,
            sparql_username,
            sparql_password,
            "parameters_by_foi_list",
            get_query("parameters_by_foi_by_site", dataset),
            process_by_foi_by_site,
            temp_dir,
        )

    def create_all_sites_tree():
        return create_sites_tree(sites_tree_dict)

    with ThreadPoolExecutor() as executor:
        future_site_tags = executor.submit(fetch_site_tags)
        future_species_by_site = executor.submit(fetch_species_by_site)
        future_classification_by_site = executor.submit(fetch_classification_by_site)
        future_fois_by_site = executor.submit(fetch_fois_by_site)
        future_parameters_by_site = executor.submit(fetch_parameters_by_site)
        future_procedures_by_site = executor.submit(fetch_procedures_by_site)
        future_procedures_by_foi_by_site = executor.submit(fetch_parameters_by_foi_by_site)
        future_all_sites_tree = executor.submit(create_all_sites_tree)

        results = (
            future_site_tags.result(),
            future_species_by_site.result(),
            future_classification_by_site.result(),
            future_fois_by_site.result(),
            future_parameters_by_site.result(),
            future_procedures_by_site.result(),
            future_procedures_by_foi_by_site.result(),
            future_all_sites_tree.result(),
        )

    return results


def ingest_site_data(
    dataset,
    es_hosts,
    es_pass,
    es_user,
    index_alias,
    chunk_size,
    sparql_endpoint,
    sparql_username,
    sparql_password,
    temp_dir,
    regions_dict,
    attributes_site_dict,
    attributes_site_visit_dict,
    dataset_project_tree: dict,
    attributes_dataset_dict: dict,
    site_visit_count: dict,
    sites_fts_dict: dict,
    sites_tree_dict: dict,
):
    logger = logging.getLogger(__name__)

    (
        site_tags,
        species_by_site,
        classification_by_site,
        fois_by_site,
        parameters_by_site,
        procedures_by_site,
        parameters_by_foi_by_site,
        all_sites_tree,
    ) = fetch_and_process_site(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        dataset,
        temp_dir,
        sites_tree_dict,
    )

    types = [
        "http://linked.data.gov.au/def/tern-cv/8cadf069-01d7-4420-b454-cae37740c2a2",  # plot
        "http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4",  # site
        "http://linked.data.gov.au/def/tern-cv/de46fa49-d1c9-4bef-8462-d7ee5174e1e1",  # transect
        "http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c",  # parent site
        "http://linked.data.gov.au/def/tern-cv/4362c8f2-b3cc-4816-b5a2-fb7bb4c0cff5",  # quadrat
    ]

    pre_site_docs = []
    for plot_type in types:
        query = get_query("site_and_location", dataset, None, None, plot_type)

        logger.info(f"Executing sparql query against endpoint: {sparql_endpoint}")
        start_time = time.time()
        temp_filename = run_sparql_query(
            sparql_endpoint, sparql_username, sparql_password, query, temp_dir
        )
        logger.info(f"Completed query in {time.time() - start_time:.2f} seconds.")

        pre_site_docs_aux = process_sites(
            temp_filename,
            regions_dict,
            attributes_site_dict,
            attributes_site_visit_dict,
            dataset_project_tree,
            attributes_dataset_dict,
            site_tags,
            site_visit_count,
            sites_fts_dict,
            all_sites_tree,
            species_by_site,
            classification_by_site,
            fois_by_site,
            parameters_by_site,
            procedures_by_site,
            parameters_by_foi_by_site,
        )

        pre_site_docs = pre_site_docs + pre_site_docs_aux

    pre_site_docs.sort(key=lambda doc: doc.get("id"))

    visits_grouped = {}
    for _, grouped_docs in groupby(pre_site_docs, key=lambda doc: doc["site_id"]):
        new_group = []
        for doc in grouped_docs:
            site_visit = extract_params(SITE_VISIT_KEYS, doc)
            if site_visit["site_visit_id"]:
                site_id = doc["site_id"]
                if site_id not in visits_grouped:
                    new_group.append(site_visit)
                    visits_grouped[site_id] = new_group
                else:
                    visits_grouped[site_id].append(site_visit)

    site_docs = []
    for doc in pre_site_docs:
        if doc["site_id"] in visits_grouped:
            doc["site_visits"] = visits_grouped[doc["site_id"]]
        site_docs.append(doc)

    # Create new index {alias}-{dataset}-{indexing_time}
    index_name = f"{index_alias}-{dataset}_{datetime.now().strftime('%Y%m%d%H%M%S')}"
    es = get_es_connection(es_hosts, es_user, es_pass)
    es.indices.create(index_name, body=site_mapping)
    es.indices.put_settings({"index": {"refresh_interval": "-1"}}, index=index_name)

    logger.info(f"Starting bulk indexing in ES: {es_hosts}")
    for index_actions in generate_sites(index_name, site_docs):
        update_index(es, index_actions, chunk_size)

    try:
        es.indices.forcemerge(index_name, max_num_segments=1)
        es.indices.refresh(index_name)
        switch_index_in_alias(es, index_alias, dataset, None, index_name)
    except exceptions.ConnectionTimeout as e:
        retry_seconds = 30
        while retry_seconds < 500:
            try:
                es.indices.forcemerge(index_name, max_num_segments=1)
                es.indices.refresh(index_name)
                switch_index_in_alias(es, index_alias, dataset, None, index_name)
                break
            except exceptions.ConnectionTimeout as e:
                logger.warning(f"ReadTimeoutError... retrying again in {retry_seconds}s")
                time.sleep(retry_seconds)
                retry_seconds *= 2
                continue

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    logger.info(f"Completed indexing in {time.time() - start_time:.2f} seconds.")


def ingest_labels(
    dataset,
    es_hosts,
    es_pass,
    es_user,
    facet,
    index_alias,
    sparql_endpoint,
    sparql_username,
    sparql_password,
    sparql_query,
    temp_dir,
):
    logger = logging.getLogger(__name__)
    logger.info(f"Executing sparql label query against endpoint: {sparql_endpoint}")
    start_time = time.time()
    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        sparql_query,
        temp_dir,
    )
    logger.info(f"Completed query in {time.time() - start_time:.2f} seconds.")

    index_name = f"{index_alias}-{dataset}_{datetime.now().strftime('%Y%m%d%H%M%S')}"
    es = get_es_connection(es_hosts, es_user, es_pass)

    first_batch = True
    logger.info(f"Starting bulk indexing in ES: {es_hosts}")
    for index_actions in generate_labels_actions(
        index_name,
        temp_filename,
        one_item_key=facet == "dataset",
    ):
        if len(index_actions) > 0:
            if first_batch:
                es.indices.create(index_name, body=label_mapping)
                es.indices.put_settings({"index": {"refresh_interval": "-1"}}, index=index_name)
                first_batch = False
            update_index(es, index_actions)

    if not first_batch:
        try:
            es.indices.forcemerge(index_name, max_num_segments=1)
            es.indices.refresh(index_name)
            switch_index_in_alias(es, index_alias, dataset, None, index_name)
        except exceptions.ConnectionTimeout as e:
            retry_seconds = 30
            while retry_seconds < 500:
                try:
                    es.indices.forcemerge(index_name, max_num_segments=1)
                    es.indices.refresh(index_name)
                    switch_index_in_alias(es, index_alias, dataset, None, index_name)
                    break
                except exceptions.ConnectionTimeout as e:
                    logger.warning(f"ReadTimeoutError... retrying again in {retry_seconds}s")
                    time.sleep(retry_seconds)
                    retry_seconds *= 2
                    continue

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    logger.info(f"Completed indexing in {time.time() - start_time:.2f} seconds.")


def index_attributes_information(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    index_alias: str,
    es_hosts: str or list,
    es_user: str,
    es_pass: str,
) -> None:
    logger = logging.getLogger(__name__)
    logger.info(f"Updating [{index_alias}] index.")
    start_time = time.time()

    # Hack to make anzograph sparql queries working
    # TODO: review need of this hack in upcoming versions of Anzograph
    if not os.environ.get("PYTHONHTTPSVERIFY", "") and getattr(
        ssl, "_create_unverified_context", None
    ):
        ssl._create_default_https_context = ssl._create_unverified_context

    sparql = SPARQLWrapper(sparql_endpoint)
    if sparql_username is not None and sparql_password is not None:
        sparql.setCredentials(sparql_username, sparql_password)
    sparql.setQuery(get_query("attr_details", dataset))
    sparql.method = "POST"
    sparql.setTimeout(SPARQL_TIMEOUT)
    sparql.setReturnFormat(JSON)
    sparql_response = sparql.query().convert()

    index_name = f"{index_alias}-{dataset}_{datetime.now().strftime('%Y%m%d%H%M%S')}"
    es = get_es_connection(es_hosts, es_user, es_pass)
    es.indices.create(index_name)

    index_actions = []
    attributes = sparql_response["results"]["bindings"]
    for attr in attributes:
        index_actions.append(
            {
                "_index": index_name,
                "_id": get_value("attribute_uri", attr),
                "_source": {
                    "attribute_dataset": get_value("dataset", attr),
                    "attribute_uri": get_value("attribute_uri", attr),
                    "attribute_label": get_value("attribute_label", attr),
                    "attribute_definition": get_value("attribute_definition", attr),
                },
            }
        )

    update_index(es, index_actions)
    switch_index_in_alias(es, index_alias, dataset, None, index_name)

    logger.info(f"Completed attributes_info indexing in {time.time() - start_time:.2f} seconds.")


def index_parameters_information(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    index_alias: str,
    es_hosts: str or list,
    es_user: str,
    es_pass: str,
) -> None:
    logger = logging.getLogger(__name__)
    logger.info(f"Updating [{index_alias}] index.")
    start_time = time.time()

    # Hack to make anzograph sparql queries working
    # TODO: review need of this hack in upcoming versions of Anzograph
    if not os.environ.get("PYTHONHTTPSVERIFY", "") and getattr(
        ssl, "_create_unverified_context", None
    ):
        ssl._create_default_https_context = ssl._create_unverified_context

    sparql = SPARQLWrapper(sparql_endpoint)
    if sparql_username is not None and sparql_password is not None:
        sparql.setCredentials(sparql_username, sparql_password)
    sparql.setQuery(get_query("param_details", dataset))
    sparql.method = "POST"
    sparql.setTimeout(SPARQL_TIMEOUT)
    sparql.setReturnFormat(JSON)
    sparql_response = sparql.query().convert()

    index_name = f"{index_alias}-{dataset}_{datetime.now().strftime('%Y%m%d%H%M%S')}"
    es = get_es_connection(es_hosts, es_user, es_pass)
    es.indices.create(index_name)

    index_actions = []
    attributes = sparql_response["results"]["bindings"]
    for attr in attributes:
        index_actions.append(
            {
                "_index": index_name,
                "_id": get_value("observed_property_uri", attr),
                "_source": {
                    "observed_property_dataset": get_value("dataset", attr),
                    "observed_property_uri": get_value("observed_property_uri", attr),
                    "observed_property_label": get_value("observed_property_label", attr),
                    "observed_property_definition": get_value("observed_property_definition", attr),
                },
            }
        )

    update_index(es, index_actions)
    switch_index_in_alias(es, index_alias, dataset, None, index_name)

    logger.info(f"Completed parameters_info indexing in {time.time() - start_time:.2f} seconds.")


def run_sparql_query(sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir):
    # Hack to make anzograph sparql queries working
    # TODO: review need of this hack in upcoming versions of Anzograph
    if not os.environ.get("PYTHONHTTPSVERIFY", "") and getattr(
        ssl, "_create_unverified_context", None
    ):
        ssl._create_default_https_context = ssl._create_unverified_context

    sparql = SPARQLWrapper(sparql_endpoint)
    if sparql_username is not None and sparql_password is not None:
        sparql.setCredentials(sparql_username, sparql_password)
    sparql.setQuery(sparql_query)
    sparql.method = "POST"
    sparql.setTimeout(SPARQL_TIMEOUT)
    sparql.setReturnFormat(JSON)
    response = sparql.query()
    temp_filename = f"{temp_dir}/{uuid.uuid4()}.json"
    with open(temp_filename, mode="wb") as localfile:
        while response.response.fp is not None:
            localfile.write(response.response.read(MAX_MEMORY_CHUNK_SIZE))
    return temp_filename


def get_ufoi(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    temp_dir: str,
    # occurrence_data: bool = False,
):
    """
    Runs the site_ufoi_qry to get all UFOIs linked to all the sites for one dataset.
    :return: ufois dict, containing all the UFOIs for each site.
    """

    logging.info(f"Querying UFOI data.")

    regions = {}
    region_types = {}

    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("ufoi_sites", dataset),
        temp_dir,
    )
    flatten_ufoi(temp_filename, regions, region_types)
    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    op_count = 3000000
    if op_count > 0:
        batches = math.ceil(op_count / SPARQL_BATCH)
        for i in range(0, batches):
            pagination = (
                f"LIMIT {SPARQL_BATCH} {f'OFFSET {SPARQL_BATCH*i}' if i>0 else ''}"
                if batches > 1
                else ""
            )
            temp_filename = run_sparql_query(
                sparql_endpoint,
                sparql_username,
                sparql_password,
                get_query("ufoi_obs", dataset, None, None, None, pagination),
                temp_dir,
            )
            flatten_ufoi(temp_filename, regions, region_types)
            if os.path.exists(temp_filename):
                os.remove(temp_filename)
            else:
                print(f"'{temp_filename}' json file does not exists")

    logging.info(f"Creation of UFOIs dictionary from sparql response completed.")

    return {"regions": regions, "region_types": region_types}


def get_site_tags(
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_XXXX_attributes to get all attributes linked to all the XXXX.
    :return: attributes dict, containing all the attributes for each XXXX.
    """

    logging.info(f"Querying site tags.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    tags = flatten_tags(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return tags


# def get_species_by_site(
#     sparql_endpoint: str,
#     sparql_username: str,
#     sparql_password: str,
#     sparql_query: str,
#     temp_dir: str,
# ):
#     """
#     Runs the query_XXXX_attributes to get all attributes linked to all the XXXX.
#     :return: attributes dict, containing all the attributes for each XXXX.
#     """

#     logging.info(f"Querying species_by_sites.")

#     temp_filename = run_sparql_query(
#         sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
#     )
#     species = flatten_species(temp_filename)

#     if os.path.exists(temp_filename):
#         os.remove(temp_filename)
#     else:
#         print(f"'{temp_filename}' json file does not exists")

#     return species


# def get_classification_by_site(
#     sparql_endpoint: str,
#     sparql_username: str,
#     sparql_password: str,
#     sparql_query: str,
#     temp_dir: str,
# ):
#     """
#     Runs the query_XXXX_attributes to get all attributes linked to all the XXXX.
#     :return: attributes dict, containing all the attributes for each XXXX.
#     """

#     logging.info(f"Querying classification_by_site.")

#     temp_filename = run_sparql_query(
#         sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
#     )
#     classification = flatten_classification(temp_filename)

#     if os.path.exists(temp_filename):
#         os.remove(temp_filename)
#     else:
#         print(f"'{temp_filename}' json file does not exists")

#     return classification


def get_resource_by_site(
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    resource: str,
    sparql_query: str,
    process_fn: Callable,
    temp_dir: str,
):
    logging.info(f"Querying {resource} by site.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    resource_processed = process_fn(temp_filename, resource)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return resource_processed


def get_attributes(
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    type: str,
    temp_dir: str,
):
    """
    Runs the query_XXXX_attributes to get all attributes linked to all the XXXX.
    :return: attributes dict, containing all the attributes for each XXXX.
    """

    logging.info(f"Querying {type} attributes per XXX data.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    attr = flatten_attributes(temp_filename, type)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return attr


def get_sampling(
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_XXXX_attributes to get all attributes linked to all the XXXX.
    :return: attributes dict, containing all the attributes for each XXXX.
    """

    logging.info(f"Querying Samplers per XXX data.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    samplers = flatten_sampling(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return samplers


def get_visit_count(
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_visit_count to get a count of site_visits
    """

    logging.info(f"Querying site_visit count.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    count = flatten_site_visit_count(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return count


def get_dataset_project_tree(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    temp_dir: str,
):
    """
    Runs the query_project_XXX to get all datasets and their projects.
    :return: dataset_project dict.
    """

    logging.info(f"Querying datasets and projects tree.")

    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("dataset_project", dataset),
        temp_dir,
    )
    dataset_tree_dict = create_dataset_project_tree(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return dataset_tree_dict


def get_dataset_citations(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    temp_dir: str,
):
    """
    Runs the query_project_XXX to get all datasets and their projects.
    :return: dataset_project dict.
    """

    logging.info(f"Querying datasets and projects tree.")

    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("dataset_citation", dataset),
        temp_dir,
    )
    dataset_citations = flatten_citations(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return dataset_citations


def get_dataset_authors(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    temp_dir: str,
):
    """
    Runs the query_project_XXX to get all datasets and their projects.
    :return: dataset_project dict.
    """

    logging.info(f"Querying datasets and projects authors/coAuthors")

    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("dataset_authors", dataset),
        temp_dir,
    )
    dataset_authors, dataset_coauthors, dataset_publishers = flatten_authors(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return dataset_authors, dataset_coauthors, dataset_publishers


def get_dataset_metadata(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    temp_dir: str,
):
    """
    Runs the query_project_XXX to get all datasets and their projects.
    :return: dataset_project dict.
    """

    logging.info(f"Querying datasets and projects metadata")

    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("dataset_metadata", dataset),
        temp_dir,
    )
    dataset_metadata = flatten_metadata(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return dataset_metadata


def get_dataset_creators(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    temp_dir: str,
):
    """
    Runs the dataset_creators to get all creators of the datasets.
    :return: dataset_creators dict.
    """

    logging.info(f"Querying dataset creators")

    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("dataset_creators", dataset),
        temp_dir,
    )
    dataset_creators = flatten_creators(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return dataset_creators


def get_site_or_visit_tree(
    type: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_XXXX_attributes to get all attributes linked to all the XXXX.
    :return: attributes dict, containing all the attributes for each XXXX.
    """

    logging.info(f"Querying sites tree.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    sites_tree_dict = create_site_or_visit_tree(type, temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return sites_tree_dict


def get_site_or_visit_label(
    type: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_XXXX_attributes to get all attributes linked to all the XXXX.
    :return: attributes dict, containing all the attributes for each XXXX.
    """

    logging.info(f"Querying sites label.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    sites_label_dict = create_site_or_visit_label(type, temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return sites_label_dict


def get_site_or_visit_fts(
    type: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_XXXX_attributes to get all attributes linked to all the XXXX.
    :return: attributes dict, containing all the attributes for each XXXX.
    """

    logging.info(f"Querying sites label.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    sites_label_dict = create_site_or_visit_fts(type, temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return sites_label_dict


def get_sites_coordinates(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    temp_dir: str,
):
    """
    Runs the query_XXXX_attributes to get all attributes linked to all the XXXX.
    :return: attributes dict, containing all the attributes for each XXXX.
    """

    logging.info(f"Querying sites coordinates.")

    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("site_coordinates", dataset),
        temp_dir,
    )
    sites_coord_dict = create_sites_coordinates(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return sites_coord_dict


def get_feature_of_interest(
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_fois to get all fois contained in the data.
    :return: list of feature of interest
    """

    logging.info(f"Querying FOIs")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    fois = get_list_fois(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return fois


def get_parameters(
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_fois to get all fois contained in the data.
    :return: list of feature of interest
    """

    logging.info(f"Querying FOIs")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    params = get_list_params(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return params


def get_site_with_data(
    dataset: str,
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    temp_dir: str,
):
    """
    Runs the query_fois to get all fois contained in the data.
    :return: list of feature of interest
    """

    logging.info(f"Querying FOIs")

    temp_filename = run_sparql_query(
        sparql_endpoint,
        sparql_username,
        sparql_password,
        get_query("site_id_with_data", dataset),
        temp_dir,
    )
    fois = get_list_sites_w_data(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return fois


def extract_params(keys, new_document):
    extracted = {}
    for key in keys:
        if key in new_document:
            extracted.update({key: new_document.get(key)})

    return extracted


def get_species_name(
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_SPECIES_foi to get all species_name by foi
    :return: species dict, containing all the species for each foi.
    """

    logging.info(f"Querying scientific name.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    species = get_species(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return species


def get_species_and_classification(
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_SPECIES_foi to get all species_name by foi
    :return: species dict, containing all the species for each foi.
    """

    logging.info(f"Querying scientific name.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    species, classification = get_species_classification(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return species, classification


def get_all_taxon(
    sparql_endpoint: str,
    sparql_username: str,
    sparql_password: str,
    sparql_query: str,
    temp_dir: str,
):
    """
    Runs the query_SPECIES_foi to get all species_name by foi
    :return: species dict, containing all the species for each foi.
    """

    logging.info(f"Querying taxon.")

    temp_filename = run_sparql_query(
        sparql_endpoint, sparql_username, sparql_password, sparql_query, temp_dir
    )
    taxon = get_taxon(temp_filename)

    if os.path.exists(temp_filename):
        os.remove(temp_filename)
    else:
        print(f"'{temp_filename}' json file does not exists")

    return taxon
