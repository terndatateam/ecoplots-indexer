import json
import logging
import re
from typing import Any

import ijson
from anytree import AnyNode, find_by_attr
from anytree.exporter import DictExporter

TERN_CV_PREFIX = "http://linked.data.gov.au/def/tern-cv/"
TERN_CV_SHORT = "tern:"
REGION_TYPE_PREFIX = "http://linked.data.gov.au/dataset/"
REGION_TYPE_SHORT = "region:"


class SiteNode(AnyNode):
    separator = "@"

    def __init__(self, site_id, site_id_label="", site_type="root", parent=None, children=None):
        self.site_id = site_id
        self.site_id_label = site_id_label
        self.site_type = site_type
        self.parent = parent
        if children:
            self.children = children


def find(_list: list, key: str, value: Any):
    for _dict in _list:
        if _dict.get(key) is not None and _dict[key] == value:
            return _dict
    return None


def get_value(key: str, row: dict):
    try:
        return row[key]["value"]
    except KeyError:
        return None


def get_datatype_value(row, column):
    try:
        if row.get(column):
            if row[column].get("datatype"):
                if row[column]["datatype"] in [
                    "http://www.w3.org/2001/XMLSchema#double",
                    "http://www.w3.org/2001/XMLSchema#decimal",
                ]:
                    return {
                        # "label": None,
                        "value": float(get_value(column, row)),
                        "type": "float",
                    }
                elif row[column]["datatype"] == "http://www.w3.org/2001/XMLSchema#boolean":
                    return {
                        # "label": None,
                        "value": strtobool(get_value(column, row)),
                        "type": "boolean",
                    }
                elif row[column]["datatype"] in [
                    "http://www.w3.org/2001/XMLSchema#int",
                    "http://www.w3.org/2001/XMLSchema#integer",
                ]:
                    return {
                        # "label": None,
                        "value": int(get_value(column, row)),
                        "type": "int",
                    }
                elif row[column]["datatype"] == "http://www.w3.org/2001/XMLSchema#dateTime":
                    return {
                        "value": get_value(column, row),
                        "type": "dateTime",
                    }
                elif row[column]["datatype"] == "http://www.w3.org/2001/XMLSchema#date":
                    return {
                        "value": get_value(column, row),
                        "type": "date",
                    }
                elif row[column]["datatype"] == "http://www.w3.org/2001/XMLSchema#string":
                    return {
                        "value": str(get_value(column, row)),
                        "type": "string",
                    }
                else:
                    raise NotImplementedError(f'Datatype: {row[column]["datatype"]}')
            else:
                label_1 = get_value(f"{column}_label_1", row)
                label_2 = get_value(f"{column}_label_2", row)
                if label_1 or label_2:
                    return {
                        "label": label_1 or label_2,
                        "value": get_value(column, row),
                        "type": "uri",
                    }
                else:
                    return {
                        # "label": None,
                        "value": str(get_value(column, row)),
                        "type": "string",
                    }
        else:
            return None
    except Exception as e:
        print(json.dumps(row, indent=2))
        raise


def get_taxon_value(row):
    try:
        return {
            "label": get_value("taxon_label", row),
            "taxon_value": {
                "taxon_accepted_name_label": get_value("taxon_accepted_name_label", row),
                "taxon_accepted_name": get_value("taxon_accepted_name", row),
                "taxon_higher_classification": get_value("taxon_higher_classification", row),
                "taxon_rank": get_value("taxon_rank", row),
                "taxon_class": get_value("taxon_class", row),
                "taxon_family": get_value("taxon_family", row),
                "taxon_genus": get_value("taxon_genus", row),
                "taxon_kingdom": get_value("taxon_kingdom", row),
                "taxon_order": get_value("taxon_order", row),
                "taxon_phylum": get_value("taxon_phylum", row),
                "taxon_vernacular_name": get_value("taxon_vernacular_name", row),
                "taxon_name_according_to_label": get_value("taxon_name_according_to_label", row),
                "taxon_name_according_to": get_value("taxon_name_according_to", row),
                "taxon_nomenclatural_code": get_value("taxon_nomenclatural_code", row),
                "taxon_nomenclatural_status": get_value("taxon_nomenclatural_status", row),
                "taxon_scientific_name": get_value("taxon_scientific_name", row),
                "taxon_scientific_name_authorship": get_value(
                    "taxon_scientific_name_authorship", row
                ),
                "taxon_scientific_name_label": get_value("taxon_scientific_name_label", row),
                "taxon_concept_id": get_value("taxon_concept_id", row),
                "taxon_taxon_id": get_value("taxon_taxon_id", row),
                "taxon_taxonomic_status": get_value("taxon_taxonomic_status", row),
            },
            "type": "taxon",
        }
    except Exception as e:
        print(json.dumps(row, indent=2))
        raise


def get_attr_datatype_value(row, column):
    try:
        if row.get(column):
            if row[column].get("datatype"):
                if row[column]["datatype"] in [
                    "http://www.w3.org/2001/XMLSchema#double",
                    "http://www.w3.org/2001/XMLSchema#decimal",
                ]:
                    return {
                        # "label": None,
                        "value_float": float(get_value(column, row)),
                        "type": "float",
                    }
                elif row[column]["datatype"] == "http://www.w3.org/2001/XMLSchema#boolean":
                    return {
                        # "label": None,
                        "value_boolean": strtobool(get_value(column, row)),
                        "type": "boolean",
                    }
                elif row[column]["datatype"] in [
                    "http://www.w3.org/2001/XMLSchema#int",
                    "http://www.w3.org/2001/XMLSchema#integer",
                ]:
                    return {
                        # "label": None,
                        "value_int": int(get_value(column, row)),
                        "type": "int",
                    }
                elif row[column]["datatype"] == "http://www.w3.org/2001/XMLSchema#dateTime":
                    return {
                        "value_datetime": get_value(column, row),
                        "type": "datetime",
                    }
                elif row[column]["datatype"] == "http://www.w3.org/2001/XMLSchema#date":
                    return {
                        "value_date": get_value(column, row),
                        "type": "date",
                    }
                elif row[column]["datatype"] == "http://www.w3.org/2001/XMLSchema#string":
                    return {
                        # "label": None,
                        "value_string": str(get_value(column, row)),
                        "type": "string",
                    }
                else:
                    raise NotImplementedError(f'Datatype: {row[column]["datatype"]}')
            else:
                label_1 = get_value(f"{column}_label_1", row)
                label_2 = get_value(f"{column}_label_2", row)
                if label_1 or label_2:
                    return {
                        "label": label_1 or label_2,
                        "value_uri": get_value(column, row),
                        "type": "uri",
                    }
                else:
                    return {
                        # "label": None,
                        "value_string": str(get_value(column, row)),
                        "type": "string",
                    }
        else:
            return None
    except Exception as e:
        print(json.dumps(row, indent=2))
        raise


def generate_observation_actions(
    index_name: str,
    data_file: str,
    regions_dict: dict,
    attributes_foi_dict: dict,
    attributes_obs_dict: dict,
    attributes_system_dict: dict,
    attributes_site_dict: dict,
    attributes_site_visit_dict: dict,
    attributes_sampling_dict: dict,
    dataset_project_tree: dict,
    attributes_dataset_dict: dict,
    sites_tree_dict: dict,
    sites_fts_dict: dict,
    visits_fts_dict: dict,
    site_visit_count: dict,
    sites_location_dict: dict,
    scientific_name_dict: dict,
    accepted_name_dict: dict,
    classification_dict: dict,
    taxon_dict: dict,
    sampling_dict: dict,
    bulk_size: int = 2000,
):
    with open(data_file, "rb") as jfile:
        ijson_docs = ijson.items(jfile, "results.bindings.item")
        index_actions = []
        docs_count = 0
        total_count = 0
        sites_tree_calculated = {}
        for row in ijson_docs:
            site_id = get_value("site_id", row)
            if site_id not in sites_tree_calculated:
                sites_tree = []
                get_site_or_visit_hierarchy(
                    "site", sites_fts_dict, sites_tree, sites_tree_dict, site_id
                )
                sites_tree_calculated.update({site_id: sites_tree})
            else:
                sites_tree = sites_tree_calculated[site_id]

            top_site_id = next((x["site_id"] for x in sites_tree if x["depth"] == 0))
            if top_site_id == get_value("top_parent_site_id", row) and len(sites_tree) > 1:
                top_site_id = next((x["site_id"] for x in sites_tree if x["depth"] == 1))

            dataset_uri = get_value("dataset", row)
            if dataset_project_tree[dataset_uri][1]:
                dataset = dataset_project_tree[dataset_uri][1]
                project = dataset_uri
            else:
                dataset = dataset_uri
                project = None

            result = (
                taxon_dict.get(get_value("taxon_taxon_id", row))
                if "taxon_taxon_id" in row
                else get_datatype_value(row, "result_value")
            )

            if (
                get_value("site_type", row)
                == "http://linked.data.gov.au/def/tern-cv/ffbe3c8c-23f1-4fc4-8aaf-dfba9f12576c"
            ):
                top_parent_site_id = site_id
            else:
                top_parent_site_id = get_value("top_parent_site_id", row)

            index_doc = {
                "_index": index_name,
                "_id": f"{get_value('id', row)}-{total_count}",
                "_source": {
                    "id": get_value("id", row),
                    "observation_class": get_value("observation_class", row),
                    "dataset": dataset,
                    "dataset_attr_count": len(attributes_dataset_dict.get(dataset, [])),
                    "project": project,
                    "project_attr_count": len(attributes_dataset_dict.get(project, [])),
                    "site_id": top_site_id,
                    "site_id_label": sites_fts_dict.get(top_site_id)["site_id_label"],
                    "direct_site_id": get_value("site_id", row),
                    "top_parent_site_id": top_parent_site_id,
                    "site_attr_count": len(attributes_site_dict.get(top_site_id, [])),
                    "sites_hierarchy": sites_tree,
                    "site_visit_count": site_visit_count.get(top_site_id),
                    "site_visit_id": get_value("site_visit_id", row),
                    "site_visit_id_label": visits_fts_dict.get(get_value("site_visit_id", row))[
                        "visit_id_label"
                    ],
                    "site_visit_attr_count": len(
                        attributes_site_visit_dict.get(get_value("site_visit_id", row), [])
                    ),
                    "site_visit_date": get_datatype_value(row, "site_visit_date"),
                    "latitude": sites_location_dict.get(top_site_id, {}).get("latitude", None),
                    "longitude": sites_location_dict.get(top_site_id, {}).get("longitude", None),
                    "altitude": sites_location_dict.get(top_site_id, {}).get("altitude", None),
                    "elevation": sites_location_dict.get(top_site_id, {}).get("elevation", None),
                    "geopoint": f"""{sites_location_dict.get(top_site_id, {}).get("latitude", None)},{sites_location_dict.get(top_site_id, {}).get("longitude", None)}""",
                    "wkt_point": sites_location_dict.get(top_site_id, {}).get("wkt_point", None),
                    "feature_id": get_value("feature_id", row),
                    "parent_feature_id": get_value("parent_feature_id", row),
                    "feature_class": get_value("feature_class", row),
                    "feature_type": get_value("feature_type", row),
                    "observed_property": get_value("observed_property", row),
                    "used_procedure": get_value("used_procedure", row),
                    "result_value": result,
                    "unit_of_measure": get_value("unit_of_measure", row),
                    "result_time": get_datatype_value(row, "result_time"),
                    "region_types": regions_dict["region_types"].get(top_site_id, []),
                    "scientific_name": scientific_name_dict.get(get_value("feature_id", row)),
                    "accepted_name": accepted_name_dict.get(get_value("feature_id", row)),
                    "species_classification": classification_dict.get(get_value("feature_id", row)),
                },
            }

            system_id = get_value("system_id", row)
            new_sampling_attr = set()
            if get_value("feature_id", row) in sampling_dict:
                system = sampling_dict.get(get_value("feature_id", row))
                system_id = (
                    system["system_id"] if system["system_id"] else get_value("system_id", row)
                )
                index_doc["_source"]["sampling_id"] = system["sampling_id"]
                index_doc["_source"]["material_sample_id"] = system["material_sample_id"]
                index_doc["_source"]["sampling_method"] = system["sampling_method"]
                index_doc["_source"]["system_id"] = system["system_id"]
                index_doc["_source"]["system_type"] = system["system_type"]
                index_doc["_source"]["system_class"] = system["system_class"]

                for attr in attributes_sampling_dict.get(system["sampling_id"], []):
                    attr_uri = attr["attribute"]
                    new_sampling_attr.add(attr_uri)
                    index_doc["_source"][
                        f"sampling_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"
                    ] = attr
                index_doc["_source"]["sampling_attributes"] = list(new_sampling_attr)
            else:
                index_doc["_source"]["material_sample_id"] = get_value("material_sample_id", row)
                index_doc["_source"]["system_id"] = get_value("system_id", row)
                index_doc["_source"]["system_type"] = get_value("system_type", row)
                index_doc["_source"]["system_class"] = get_value("system_class", row)

            tags = get_value("tags", row).split(",") if get_value("tags", row) else []
            for search_field in sites_fts_dict.get(get_value("site_id", row)).values():
                tags.append(search_field)
            for search_field in visits_fts_dict.get(get_value("site_visit_id", row)).values():
                tags.append(search_field)
            tags.append(get_value("used_procedure_label", row))
            if result.get("label"):
                tags.append(result.get("label"))

            if dataset_project_tree[dataset_uri][2]:
                tags.append(dataset_project_tree[dataset_uri][0])
                tags.append(dataset_project_tree[dataset_uri][2])
            else:
                tags.append(dataset_project_tree[dataset_uri][0])

            index_doc["_source"]["tags"] = tags

            for region_tuple in regions_dict["regions"].get(top_site_id, []):
                index_doc["_source"][
                    f"{region_tuple[0].replace(REGION_TYPE_PREFIX, REGION_TYPE_SHORT)}"
                ] = region_tuple[1]

            new_foi_attr = set()
            for attr in attributes_foi_dict.get(get_value("feature_id", row), []):
                attr_uri = attr["attribute"]
                new_foi_attr.add(attr_uri)
                index_doc["_source"][
                    f"foi_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"
                ] = attr
            index_doc["_source"]["foi_attributes"] = list(new_foi_attr)

            new_obs_attr = set()
            for attr in attributes_obs_dict.get(get_value("id", row), []):
                attr_uri = attr["attribute"]
                new_obs_attr.add(attr_uri)
                index_doc["_source"][
                    f"obs_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"
                ] = attr
            index_doc["_source"]["obs_attributes"] = list(new_obs_attr)

            new_system_attr = set()
            for attr in attributes_system_dict.get(system_id, []):
                attr_uri = attr["attribute"]
                new_system_attr.add(attr_uri)
                index_doc["_source"][
                    f"system_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"
                ] = attr
            index_doc["_source"]["system_attributes"] = list(new_system_attr)

            index_actions.append(index_doc)
            docs_count += 1
            total_count += 1
            if docs_count == bulk_size:
                send_actions = index_actions
                docs_count = 0
                index_actions = []
                yield send_actions

        yield index_actions

    return index_actions


def generate_obs_occurrence_actions(
    index_name: str,
    data_file: str,
    regions_dict: dict,
    attributes_foi_dict: dict,
    attributes_obs_dict: dict,
    attributes_system_dict: dict,
    attributes_sampling_dict: dict,
    dataset_project_tree: dict,
    attributes_dataset_dict: dict,
    scientific_name_dict: dict,
    accepted_name_dict: dict,
    classification_dict: dict,
    taxon_dict: dict,
    sampling_dict: dict,
    bulk_size: int = 2000,
):
    with open(data_file, "rb") as jfile:
        ijson_docs = ijson.items(jfile, "results.bindings.item")
        index_actions = []
        docs_count = 0
        total_count = 0
        for row in ijson_docs:
            obs_id = get_value("id", row)
            dataset_uri = get_value("dataset", row)
            if dataset_project_tree[dataset_uri][1]:
                dataset = dataset_project_tree[dataset_uri][1]
                project = dataset_uri
            else:
                dataset = dataset_uri
                project = None

            result = (
                taxon_dict.get(get_value("taxon_taxon_id", row))
                if "taxon_taxon_id" in row
                else get_datatype_value(row, "result_value")
            )

            index_doc = {
                "_index": index_name,
                "_id": f"{obs_id}-{total_count}-occurr",
                "_source": {
                    "id": obs_id,
                    "observation_class": get_value("observation_class", row),
                    "dataset": dataset,
                    "dataset_attr_count": len(attributes_dataset_dict.get(dataset, [])),
                    "project": project,
                    "project_attr_count": len(attributes_dataset_dict.get(project, [])),
                    "feature_id": get_value("feature_id", row),
                    "parent_feature_id": get_value("parent_feature_id", row),
                    "feature_class": get_value("feature_class", row),
                    "feature_type": get_value("feature_type", row),
                    "latitude": get_value("latitude", row),
                    "longitude": get_value("longitude", row),
                    "altitude": get_value("altitude", row),
                    "elevation": get_value("elevation", row),
                    "geopoint": f"{get_value('latitude', row)},{get_value('longitude', row)}",
                    "wkt_point": get_value("wkt_point", row),
                    "observed_property": get_value("observed_property", row),
                    "used_procedure": get_value("used_procedure", row),
                    "result_value": result,
                    "unit_of_measure": get_value("unit_of_measure", row),
                    "result_time": get_datatype_value(row, "result_time"),
                    "region_types": regions_dict["region_types"].get(
                        get_value("wkt_point", row), []
                    ),
                    "scientific_name": scientific_name_dict.get(get_value("feature_id", row)),
                    "accepted_name": accepted_name_dict.get(get_value("feature_id", row)),
                    "species_classification": classification_dict.get(get_value("feature_id", row)),
                },
            }

            system_id = get_value("system_id", row)
            new_sampling_attr = set()
            if get_value("feature_id", row) in sampling_dict:
                system = sampling_dict.get(get_value("feature_id", row))
                system_id = (
                    system["system_id"] if system["system_id"] else get_value("system_id", row)
                )
                index_doc["_source"]["sampling_id"] = system["sampling_id"]
                index_doc["_source"]["material_sample_id"] = system["material_sample_id"]
                index_doc["_source"]["sampling_method"] = system["sampling_method"]
                index_doc["_source"]["system_id"] = system["system_id"]
                index_doc["_source"]["system_type"] = system["system_type"]
                index_doc["_source"]["system_class"] = system["system_class"]

                for attr in attributes_sampling_dict.get(system["sampling_id"], []):
                    attr_uri = attr["attribute"]
                    new_sampling_attr.add(attr_uri)
                    index_doc["_source"][
                        f"sampling_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"
                    ] = attr
                index_doc["_source"]["sampling_attributes"] = list(new_sampling_attr)
            else:
                index_doc["_source"]["material_sample_id"] = get_value("material_sample_id", row)
                index_doc["_source"]["system_id"] = get_value("sensor_id", row)
                index_doc["_source"]["system_type"] = get_value("system_type", row)
                index_doc["_source"]["system_class"] = get_value("system_class", row)

            tags = get_value("tags", row).split(",") if get_value("tags", row) else []
            tags.append(get_value("used_procedure_label", row))
            if result.get("label"):
                tags.append(result.get("label"))

            for region_tuple in regions_dict["regions"].get(get_value("wkt_point", row), []):
                index_doc["_source"][
                    f"{region_tuple[0].replace(REGION_TYPE_PREFIX, REGION_TYPE_SHORT)}"
                ] = region_tuple[1]

            new_foi_attr = set()
            for attr in attributes_foi_dict.get(get_value("feature_id", row), []):
                attr_uri = attr["attribute"]
                new_foi_attr.add(attr_uri)
                index_doc["_source"][
                    f"foi_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"
                ] = attr
            index_doc["_source"]["foi_attributes"] = list(new_foi_attr)

            new_obs_attr = set()
            for attr in attributes_obs_dict.get(obs_id, []):
                attr_uri = attr["attribute"]
                new_obs_attr.add(attr_uri)
                index_doc["_source"][
                    f"obs_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"
                ] = attr
            index_doc["_source"]["obs_attributes"] = list(new_obs_attr)

            new_system_attr = set()
            for attr in attributes_system_dict.get(system_id, []):
                attr_uri = attr["attribute"]
                new_system_attr.add(attr_uri)
                index_doc["_source"][
                    f"system_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"
                ] = attr
            index_doc["_source"]["system_attributes"] = list(new_system_attr)

            # new_sampling_attr = set()
            # if get_value("sampling_id", row):
            #     for attr in attributes_sampling_dict.get(get_value("sampling_id", row), []):
            #         attr_uri = attr["attribute"]
            #         new_sampling_attr.add(attr_uri)
            #         index_doc["_source"][
            #             f"sampling_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"
            #         ] = attr
            # index_doc["_source"]["sampling_attributes"] = list(new_sampling_attr)

            index_actions.append(index_doc)
            docs_count += 1
            total_count += 1
            if docs_count == bulk_size:
                send_actions = index_actions
                docs_count = 0
                index_actions = []
                yield send_actions

        yield index_actions

    return index_actions


def get_site_or_visit_hierarchy(type, labels, result, tree, item):
    if tree[item].get("parent_site_id"):
        level = get_site_or_visit_hierarchy(
            type, labels, result, tree, tree[item].get("parent_site_id")
        )
        result.append(
            {
                f"{type}_id": item,
                f"{type}_id_label": labels[item][f"{type}_id_label"],
                f"{type}_type": tree[item][f"{type}_type"],
                f"parent_{type}_id": tree[item].get("parent_site_id"),
                f"parent_{type}_id_label": labels[tree[item].get("parent_site_id")][
                    f"{type}_id_label"
                ],
                f"parent_{type}_type": tree[item][f"parent_{type}_type"],
                "depth": level,
            }
        )
        return level + 1
    else:
        result.append(
            {
                f"{type}_id": item,
                f"{type}_id_label": labels[item][f"{type}_id_label"],
                f"{type}_type": tree[item][f"{type}_type"],
                f"parent_{type}_id": None,
                "depth": 0,
            }
        )
        return 1


def create_sites_tree(sites):
    tree = SiteNode(site_id="root")
    for site in sites.values():
        base_node = find_by_attr(tree, name="site_id", value=site["site_id"])
        if not base_node:
            if site.get("parent_site_id"):
                parent = find_by_attr(tree, name="site_id", value=site["parent_site_id"])
                sub = next(
                    (c for c in tree.descendants if c.site_id == site["parent_site_id"]),
                    None,
                )
                if parent != sub:
                    print("ey")
                if parent:
                    # create child and add to parent
                    SiteNode(
                        site_id=site["site_id"],
                        site_id_label=site["site_id_label"],
                        site_type=site["site_type"],
                        parent=parent,
                    )
                else:
                    # create parent hierarchy recursively
                    create_branch(tree, sites, site)
            else:
                SiteNode(
                    site_id=site["site_id"],
                    site_id_label=site["site_id_label"],
                    site_type=site["site_type"],
                    parent=tree,
                )
    return tree


def create_branch(tree, sites, site_item):
    site_node = find_by_attr(tree, name="site_id", value=site_item["site_id"])
    if site_node:
        return site_node
    else:
        if not site_item.get("parent_site_id"):
            # Base case:
            # base_site = find_by_attr(tree, name="site_id", value=site_item["site_id"])
            # sub = next((c for c in tree.descendants if c.site_id == site_item["site_id"]), None)
            # if base_site != sub:
            #     print("ey")
            # if base_site:
            # return base_site
            # else:
            return SiteNode(
                site_id=site_item["site_id"],
                site_id_label=site_item["site_id_label"],
                site_type=site_item["site_type"],
                parent=tree,
            )
        else:
            # General case:
            # sub = next((c for c in tree.descendants if c.site_id == site_item["site_id"]), None)
            # if site_node != sub:
            #     print("ey")
            parent = create_branch(tree, sites, sites[site_item["parent_site_id"]])
            return SiteNode(
                site_id=site_item["site_id"],
                site_id_label=site_item["site_id_label"],
                site_type=site_item["site_type"],
                parent=parent,
            )


def process_sites(
    data_file: str,
    regions_dict: dict,
    attributes_site_dict: dict,
    attributes_site_visit_dict: dict,
    dataset_project_tree: dict,
    attributes_dataset_dict: dict,
    site_tags: dict,
    site_visit_count: dict,
    sites_fts_dict: dict,
    sites_tree_dict: dict,
    species_by_site: dict,
    classification_by_site: dict,
    fois_by_site: dict,
    parameters_by_site: dict,
    procedures_by_site: dict,
    parameters_by_foi_by_site: dict,
):
    with open(data_file, "rb") as jfile:
        ijson_docs = ijson.items(jfile, "results.bindings.item")
        index_actions = []
        # sites_tree_calculated = {}
        tree_exporter = DictExporter()
        for row in ijson_docs:
            site_id = get_value("site_id", row)

            site_node = find_by_attr(sites_tree_dict, name="site_id", value=site_id)
            top_site_node = site_node.ancestors[1] if len(site_node.ancestors) > 1 else site_node

            species_list = species_by_site.get(site_node.site_id, set())
            classification_list = classification_by_site.get(site_node.site_id, set())
            fois_list = fois_by_site.get(site_node.site_id, set())
            parameters_list = parameters_by_site.get(site_node.site_id, set())
            procedures_list = procedures_by_site.get(site_node.site_id, set())
            parameters_by_foi = parameters_by_foi_by_site.get(site_node.site_id, {})
            for descendant in site_node.descendants:
                if species_by_site.get(descendant.site_id):
                    species_list = species_list.union(species_by_site.get(descendant.site_id))
                if classification_by_site.get(descendant.site_id):
                    classification_list = classification_list.union(
                        classification_by_site.get(descendant.site_id)
                    )
                # if fois_by_site.get(descendant.site_id):
                #     fois_list = fois_list.union(fois_by_site.get(descendant.site_id))
                if parameters_by_site.get(descendant.site_id):
                    parameters_list = parameters_list.union(
                        parameters_by_site.get(descendant.site_id)
                    )
                if procedures_by_site.get(descendant.site_id):
                    procedures_list = procedures_list.union(
                        procedures_by_site.get(descendant.site_id)
                    )

            dataset_uri = get_value("dataset", row)
            if dataset_project_tree[dataset_uri][1]:
                dataset = dataset_project_tree[dataset_uri][1]
                project = dataset_uri
            else:
                dataset = dataset_uri
                project = None
            index_doc = {
                "id": (
                    get_value("site_visit_id", row)
                    if get_value("site_visit_id", row)
                    else get_value("site_id", row)
                ),
                "site_class": get_value("site_class", row),
                "visit_class": get_value("visit_class", row),
                "site_visit_count": site_visit_count.get(site_id),
                "dataset": dataset,
                "dataset_attr_count": len(attributes_dataset_dict.get(dataset, [])),
                "project": project,
                "project_attr_count": len(attributes_dataset_dict.get(project, [])),
                "site_id": get_value("site_id", row),
                "site_id_label": get_value("site_id_label", row),
                "parent_site_id": get_value("parent_site_id", row),
                "parent_site_label": get_value("parent_site_label", row),
                "top_parent_site_id": get_value("top_parent_site_id", row),
                "top_parent_site_label": get_value("top_parent_site_label", row),
                "sites_hierarchy": tree_exporter.export(top_site_node),
                "date_commissioned": get_datatype_value(row, "date_commissioned"),
                "dimension": get_value("dimension", row),
                "location_description": get_value("location_description", row),
                "site_description": get_value("site_description", row),
                "feature_type": get_value("feature_type", row),
                "latitude": get_value("latitude", row),
                "longitude": get_value("longitude", row),
                "altitude": get_value("altitude", row),
                "elevation": get_value("elevation", row),
                "geopoint": f"{get_value('latitude', row)},{get_value('longitude', row)}",
                "wkt_point": get_value("wkt_point", row),
                "point_type": get_value("point_type", row),
                "wkt_polygon": get_value("wkt_polygon", row),
                "transect_direction": get_value("transect_direction", row),
                "region_types": regions_dict["region_types"].get(
                    get_value("site_id", row),
                    regions_dict["region_types"].get(get_value("parent_site_id", row), []),
                ),
                "site_visit_id": get_value("site_visit_id", row),
                "site_visit_id_label": get_value("site_visit_id_label", row),
                "visit_start_date": get_datatype_value(row, "visit_start_date"),
                "visit_end_date": get_datatype_value(row, "visit_end_date"),
                "site_visit_description": get_value("site_visit_description", row),
                "site_visit_location_description": get_value(
                    "site_visit_location_description", row
                ),
                "site_citation": get_value("site_citation", row),
                "tags": list(site_tags.get(get_value("site_id", row), set())),
                "scientific_name": list(species_list),
                "species_classification": list(classification_list),
                "feature_types": list(fois_list),
                "observed_property": list(parameters_list),
                "used_procedure": list(procedures_list),
                "feature_type_parameters": parameters_by_foi,
            }

            index_doc["tags"].append(dataset_project_tree[dataset_uri][0])
            if dataset_project_tree[dataset_uri][2]:
                index_doc["tags"].append(dataset_project_tree[dataset_uri][2])

            for region_tuple in regions_dict["regions"].get(
                get_value("site_id", row),
                regions_dict["regions"].get(get_value("parent_site_id", row), []),
            ):
                index_doc[f"{region_tuple[0].replace(REGION_TYPE_PREFIX, REGION_TYPE_SHORT)}"] = (
                    region_tuple[1]
                )

            new_site_attr = set()
            for attr in attributes_site_dict.get(get_value("site_id", row), []):
                attr_uri = attr["attribute"]
                new_site_attr.add(attr_uri)
                index_doc[f"site_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"] = attr
            index_doc["site_attributes"] = list(new_site_attr)

            new_visit_attr = set()
            for attr in attributes_site_visit_dict.get(get_value("site_visit_id", row), []):
                attr_uri = attr["attribute"]
                new_visit_attr.add(attr_uri)
                index_doc[f"visit_attr_{attr_uri.replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"] = attr
            index_doc["site_visit_attributes"] = list(new_visit_attr)

            index_actions.append(index_doc)

    return index_actions


def generate_sites(index_name: str, sites: list, bulk_size: int = 2000):
    docs_count = 0
    total_count = 0
    index_actions = []
    for site in sites:
        index_actions.append(
            {
                "_index": index_name,
                "_id": f"{site.get('id')}-{total_count}",
                "_source": site,
            }
        )

        docs_count += 1
        total_count += 1
        if docs_count == bulk_size:
            send_actions = index_actions
            docs_count = 0
            index_actions = []
            yield send_actions

    yield index_actions


def create_dataset_action(index_name: str, datasets: list, bulk_size: int = 2000):
    docs_count = 0
    total_count = 0
    index_actions = []
    for dataset in datasets:
        index_actions.append(
            {
                "_index": index_name,
                "_id": dataset["dataset_uri"],
                "_source": dataset,
            }
        )
        docs_count += 1
        total_count += 1
        if docs_count == bulk_size:
            send_actions = index_actions
            docs_count = 0
            index_actions = []
            yield send_actions

    yield index_actions


def flatten_attributes(sparql_response, type="feature_id"):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Flattening {type} Attributes response...")

    entity = {}

    f = open(sparql_response, "rb")
    docs = ijson.items(f, "results.bindings.item")

    for row in docs:
        entity_id = row[type]["value"]
        if entity_id in entity.keys():
            entity.get(entity_id).append(
                {
                    "id": get_value("id", row),
                    "attribute": get_value("attribute", row),
                    "value": get_attr_datatype_value(row, "value"),
                    "unit_of_measure": get_value("unit_of_measure", row),
                }
            )
        else:
            entity[entity_id] = [
                {
                    "id": get_value("id", row),
                    "attribute": get_value("attribute", row),
                    "value": get_attr_datatype_value(row, "value"),
                    "unit_of_measure": get_value("unit_of_measure", row),
                }
            ]

    f.close()

    logging.info(f"Creation of {type} attributes dictionary from sparql response completed.")

    return entity


def flatten_site_visit_count(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Generating SiteVisit count...")

    count = {}

    f = open(sparql_response, "rb")
    docs = ijson.items(f, "results.bindings.item")

    for row in docs:
        site_id = row["site_id"]["value"]
        count[site_id] = row["site_visit_count"]["value"]

    f.close()

    logging.info(f"Creation of {type} attributes dictionary from sparql response completed.")

    return count


def flatten_sampling(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Generating Samplers by FOI dict...")

    samplers = {}

    f = open(sparql_response, "rb")
    docs = ijson.items(f, "results.bindings.item")

    for row in docs:
        feature_id = row["feature_id"]["value"]
        samplers[feature_id] = {
            "sampling_id": row["sampling_id"]["value"],
            "sampling_method": row["sampling_method"]["value"],
            "material_sample_id": get_value("material_sample_id", row),
            "system_id": get_value("system_id", row),
            "system_class": get_value("system_class", row),
            "system_type": get_value("system_type", row),
        }

    f.close()

    logging.info(f"Creation of {type} attributes dictionary from sparql response completed.")

    return samplers


def flatten_ufoi(sparql_response, regions, region_types):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Flattening UFOI response...")

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        id = row["id"]["value"]
        region_uri = str(row["ufoi"]["value"])

        if re.search("http:\/\/linked\.data\.gov\.au\/dataset\/asgs2016\/.+", region_uri):
            dataset_uri = "http://linked.data.gov.au/dataset/asgs2016/stateorterritory"
        elif re.search(
            "http:\/\/linked\.data\.gov\.au\/dataset\/capad-2018-terrestrial\/.+",
            region_uri,
        ):
            dataset_uri = "http://linked.data.gov.au/dataset/capad-2018-terrestrial"
        elif re.search(
            "http:\/\/linked\.data\.gov\.au\/dataset\/wwf-terr-ecoregions\/.+",
            region_uri,
        ):
            dataset_uri = "http://linked.data.gov.au/dataset/wwf-terr-ecoregions"
        elif re.search(
            "http:\/\/linked\.data\.gov\.au\/dataset\/local-gov-areas-2011\/.+",
            region_uri,
        ):
            dataset_uri = "http://linked.data.gov.au/dataset/local-gov-areas-2011"
        elif re.search("http:\/\/linked\.data\.gov\.au\/dataset\/nrm-2017\/.+", region_uri):
            dataset_uri = "http://linked.data.gov.au/dataset/nrm-2017"
        elif re.search(
            "http:\/\/linked\.data\.gov\.au\/dataset\/bioregion\/[a-zA-Z]+[0-9]+",
            region_uri,
        ):
            dataset_uri = "http://linked.data.gov.au/dataset/bioregion"
        elif re.search(
            "http:\/\/linked\.data\.gov\.au\/dataset\/bioregion\/[a-zA-Z]+$", region_uri
        ):
            dataset_uri = "http://linked.data.gov.au/dataset/bioregion/IBRA7"
        else:
            dataset_uri = "unknown"

        if id in regions.keys():
            regions.get(id).append((dataset_uri, region_uri))
            region_types.get(id).append(dataset_uri)
        else:
            regions[id] = [(dataset_uri, region_uri)]
            region_types[id] = [dataset_uri]

    f.close()


def flatten_creators(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Flattening Dataset creators response...")

    creators = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        dataset_uri = row["dataset_uri"]["value"]
        dataset_creator = row["dataset_creator"]["value"]
        dataset_creator_label = row.get("dataset_creator_label", {"value": "MISSING LABEL"})[
            "value"
        ]

        if dataset_uri in creators.keys():
            creators.get(dataset_uri).append(
                {"creator": dataset_creator, "label": dataset_creator_label}
            )
        else:
            creators[dataset_uri] = [{"creator": dataset_creator, "label": dataset_creator_label}]

    f.close()

    logging.info(f"Creation of Dataset creators dictionary from sparql response completed.")

    return creators


def flatten_citations(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Flattening Site Tags response...")

    dataset_citations = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        dataset_id = row["dataset_uri"]["value"]
        citation = row["dataset_citation"]["value"].split("||")
        if len(citation) > 1:
            if dataset_id in dataset_citations.keys():
                dataset_citations.get(dataset_id).get("by_site").update({citation[0]: citation[1]})
            else:
                dataset_citations[dataset_id] = {
                    "by_dataset": [],
                    "by_site": {citation[0]: citation[1]},
                }
        else:
            if dataset_id in dataset_citations.keys():
                dataset_citations.get(dataset_id).get("by_dataset").append(citation[0])
            else:
                dataset_citations[dataset_id] = {
                    "by_dataset": [citation[0]],
                    "by_site": {},
                }

    f.close()

    logging.info(f"Creation of dataset_citation dictionary from sparql response completed.")

    return dataset_citations


def flatten_authors(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Flattening Site Tags response...")

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    authors = {}
    coAuthors = {}
    publishers = {}

    for row in docus:

        dataset_raw = row["dataset_uri"]["value"]
        dataset = dataset_raw.split("@@")

        if len(dataset) > 1:
            dataset_id = dataset[0]
            site_id = dataset[1]
            order = row["order"]["value"]
            order = int(order.split("http://www.w3.org/1999/02/22-rdf-syntax-ns#_", 1)[1])
            id = row["author"]["value"]
            name = row["name"]["value"]
            if row["type"]["value"] == "author":
                if dataset_id in authors.keys():
                    site = authors.get(dataset_id).get("by_site")
                    if site_id in site:
                        site.get(site_id).append({"order": order, "id": id, "name": name})
                        site[site_id] = sorted(site.get(site_id), key=lambda auth: auth["order"])
                    else:
                        authors.get(dataset_id).get("by_site").update(
                            {site_id: [{"order": order, "id": id, "name": name}]}
                        )
                else:
                    authors[dataset_id] = {
                        "by_dataset": [],
                        "by_site": {site_id: [{"order": order, "id": id, "name": name}]},
                    }
            elif row["type"]["value"] == "coAuthor":
                if dataset_id in coAuthors.keys():
                    site = coAuthors.get(dataset_id).get("by_site")
                    if site_id in site:
                        site.get(site_id).append({"order": order, "id": id, "name": name})
                        site[site_id] = sorted(site.get(site_id), key=lambda auth: auth["order"])
                    else:
                        coAuthors.get(dataset_id).get("by_site").update(
                            {site_id: [{"order": order, "id": id, "name": name}]}
                        )
                else:
                    coAuthors[dataset_id] = {
                        "by_dataset": [],
                        "by_site": {site_id: [{"order": order, "id": id, "name": name}]},
                    }
            elif row["type"]["value"] == "publisher":
                if dataset_id in publishers.keys():
                    publishers.get(dataset_id).get("by_dataset").update([(order, id, name)])
                else:
                    publishers[dataset_id] = {
                        "by_dataset": set([(order, id, name)]),
                        "by_site": {},
                    }
        else:
            dataset_id = dataset[0]
            order = row["order"]["value"]
            order = int(order.split("http://www.w3.org/1999/02/22-rdf-syntax-ns#_", 1)[1])
            id = row["author"]["value"]
            name = row["name"]["value"]
            if row["type"]["value"] == "author":
                if dataset_id in authors.keys():
                    authors.get(dataset_id).get("by_dataset").append(
                        {"order": order, "id": id, "name": name}
                    )
                    authors.get(dataset_id)["by_dataset"] = sorted(
                        authors.get(dataset_id).get("by_dataset"), key=lambda auth: auth["order"]
                    )
                else:
                    authors[dataset_id] = {
                        "by_dataset": [{"order": order, "id": id, "name": name}],
                        "by_site": {},
                    }
            elif row["type"]["value"] == "coAuthor":
                if dataset_id in coAuthors.keys():
                    coAuthors.get(dataset_id).get("by_dataset").append(
                        {"order": order, "id": id, "name": name}
                    )
                    coAuthors.get(dataset_id)["by_dataset"] = sorted(
                        coAuthors.get(dataset_id).get("by_dataset"), key=lambda auth: auth["order"]
                    )
                else:
                    coAuthors[dataset_id] = {
                        "by_dataset": [{"order": order, "id": id, "name": name}],
                        "by_site": {},
                    }
            elif row["type"]["value"] == "publisher":
                if dataset_id in publishers.keys():
                    publishers.get(dataset_id).get("by_dataset").update([(order, id, name)])
                else:
                    publishers[dataset_id] = {
                        "by_dataset": set([(order, id, name)]),
                        "by_site": {},
                    }

    f.close()

    logging.info(f"Creation of dataset_citation dictionary from sparql response completed.")

    return authors, coAuthors, publishers


def flatten_metadata(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Flattening Site Tags response...")

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    metadata_records = {}

    for row in docus:

        dataset_raw = row["dataset_uri"]["value"]
        dataset = dataset_raw.split("@@")

        if len(dataset) > 1:
            dataset_id = dataset[0]
            site_id = dataset[1]
            metadata = row["dataset_metadata"]["value"]
            if dataset_id in metadata_records.keys():
                site = metadata_records.get(dataset_id).get("by_site")
                if site_id in site:
                    site.get(site_id).append(metadata)
                else:
                    metadata_records.get(dataset_id).get("by_site").update({site_id: [metadata]})
            else:
                metadata_records[dataset_id] = {
                    "by_dataset": [],
                    "by_site": {site_id: [metadata]},
                }
        else:
            dataset_id = dataset[0]
            metadata = row["dataset_metadata"]["value"]
            if dataset_id in metadata_records.keys():
                metadata_records.get(dataset_id).get("by_dataset").append(metadata)
            else:
                metadata_records[dataset_id] = {
                    "by_dataset": [metadata],
                    "by_site": {},
                }

    f.close()

    logging.info(f"Creation of dataset_metadata dictionary from sparql response completed.")

    return metadata_records


def flatten_tags(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Flattening Site Tags response...")

    site_tags = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        site_id = row["site_id"]["value"]
        tags = row["tags"]["value"].split(",")
        if site_id in site_tags.keys():
            site_tags[site_id] = site_tags.get(site_id).union(set(tags))
        else:
            site_tags[site_id] = set(tags)

    f.close()

    logging.info(f"Creation of tags dictionary from sparql response completed.")

    return site_tags


def process_by_site(sparql_response, resource):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Processing {resource} by site response...")

    resource_list = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        resource_list[row["site_id"]["value"]] = set(row[resource]["value"].split("|"))

    f.close()

    logging.info(f"Creation of {resource} by site dictionary from sparql response completed.")

    return resource_list


def process_by_foi_by_site(sparql_response, resource):
    logging.info(f"Processing {resource} by site response...")

    resource_list = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        site_id = row["site_id"]["value"]
        feature_type_short = (
            f"foi_param_{row['feature_type']['value'].replace(TERN_CV_PREFIX, TERN_CV_SHORT)}"
        )
        parameters = set(row[resource]["value"].split("|"))

        if site_id not in resource_list:
            # If site_id is not present, initialize it
            resource_list[site_id] = {}
        if feature_type_short not in resource_list[site_id]:
            # If feature_type is not present for the site, initialize it
            resource_list[site_id][feature_type_short] = parameters
        else:
            # If feature_type is already present for the site, update it by adding new parameters
            resource_list[site_id][feature_type_short].update(parameters)

    f.close()

    for site_id in resource_list:
        for feature_type in resource_list[site_id]:
            resource_list[site_id][feature_type] = list(resource_list[site_id][feature_type])

    logging.info(f"Creation of {resource} by site dictionary from sparql response completed.")

    return resource_list


def flatten_species(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Flattening Species_by_site response...")

    species_list = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        species_list[row["site_id"]["value"]] = set(row["species_list"]["value"].split("|"))

    f.close()

    logging.info(f"Creation of Species_by_site dictionary from sparql response completed.")

    return species_list


def flatten_classification(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Flattening Classification_by_site response...")

    classification_list = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        site_id = row["site_id"]["value"]
        classification = row["classification_list"]["value"].split("|")
        classification_list[site_id] = set(classification)

    f.close()

    logging.info(f"Creation of Classification_by_site dictionary from sparql response completed.")

    return classification_list


def create_site_or_visit_tree(type, sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Creating {type} tree response...")

    sites = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        if f"parent_{type}_id" in row:
            sites.update(
                {
                    row[f"{type}_id"]["value"]: {
                        f"{type}_id": row[f"{type}_id"]["value"],
                        f"{type}_id_label": row[f"{type}_id_label"]["value"],
                        f"{type}_type": row[f"{type}_type"]["value"],
                        f"parent_{type}_id": row[f"parent_{type}_id"]["value"],
                        f"parent_{type}_id_label": row[f"parent_{type}_id_label"]["value"],
                        f"parent_{type}_type": row[f"parent_{type}_type"]["value"],
                    }
                }
            )
        else:
            sites.update(
                {
                    row[f"{type}_id"]["value"]: {
                        f"{type}_id": row[f"{type}_id"]["value"],
                        f"{type}_id_label": row[f"{type}_id_label"]["value"],
                        f"{type}_type": row[f"{type}_type"]["value"],
                        f"parent_{type}_id": None,
                        f"parent_{type}_id_label": None,
                        f"parent_{type}_type": None,
                    }
                }
            )
    f.close()

    logging.info(f"Creation of {type} dictionary from sparql response completed.")

    return sites


def create_dataset_project_tree(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Creating dataset tree response...")

    dataset = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        if "dataset_parent" in row:
            dataset.update(
                {
                    row["dataset_uri"]["value"]: (
                        row["dataset_label"]["value"],
                        row["dataset_parent"]["value"],
                        row["dataset_parent_label"]["value"],
                    )
                }
            )
        else:
            dataset.update(
                {
                    row["dataset_uri"]["value"]: (
                        row["dataset_label"]["value"],
                        None,
                        None,
                    )
                }
            )
    f.close()

    logging.info(f"Creation of dataset tree dictionary from sparql response completed.")

    return dataset


def create_site_or_visit_label(type, sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Processing CREATE {type} LABEL response...")

    sites = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        sites.update({row[f"{type}_id"]["value"]: row[f"{type}_id_label"]["value"]})

    f.close()

    logging.info(f"Creation CREATE {type} LABEL dictionary from sparql response completed.")

    return sites


def create_site_or_visit_fts(type, sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Processing CREATE {type} LABEL response...")

    sites = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        sites.update(
            {
                row[f"{type}_id"]["value"]: {
                    f"{type}_id_label": row[f"{type}_id_label"]["value"],
                    f"{type}_location_description": row.get(
                        f"{type}_location_description", {"value": ""}
                    )["value"],
                    f"{type}_description": row.get(f"{type}_description", {"value": ""})["value"],
                }
            }
        )

    f.close()

    logging.info(f"Creation CREATE {type} LABEL dictionary from sparql response completed.")

    return sites


def create_sites_coordinates(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Processing CREATE sites coordiantes response...")

    sites = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        sites.update(
            {
                row[f"site_id"]["value"]: {
                    f"latitude": row[f"latitude"]["value"],
                    f"longitude": row[f"longitude"]["value"],
                    f"wkt_point": row[f"wkt_point"]["value"],
                    f"elevation": get_value("elevation", row),
                    f"altitude": get_value("altitude", row),
                }
            }
        )

    f.close()

    logging.info(f"Creation CREATE {type} LABEL dictionary from sparql response completed.")

    return sites


def get_list_fois(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Processing FOIs response...")

    fois = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        fois.update({row["feature_type_key"]["value"]: row["feature_type"]["value"]})

    f.close()

    logging.info(f"Creation FOIs dictionary from sparql response completed.")

    return fois


def get_list_params(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Processing Parameters response...")

    params = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        param_id = row["observed_property"]["value"]
        count = params.get(param_id, {})
        count[row["type"]["value"]] = int(row["count"]["value"])
        params[param_id] = count

    f.close()

    logging.info(f"Creation Parameters dictionary from sparql response completed.")

    return params


def get_list_sites_w_data(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Processing FOIs response...")

    sites = []

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        sites.append(row["uri"]["value"])

    f.close()

    logging.info(f"Creation FOIs dictionary from sparql response completed.")

    return sites


def generate_labels_actions(
    index_name: str, data_file: str, one_item_key: bool = False, bulk_size: int = 10000
):
    with open(data_file, "rb") as jfile:
        ijson_docs = ijson.items(jfile, "results.bindings.item")
        index_actions = []
        docs_count = 0
        for row in ijson_docs:
            uri = get_value("uri", row)
            key = get_value("key", row)
            if not key:
                split = uri.rsplit("/", 2)
                if one_item_key:
                    key = split[-1]
                else:
                    key = f"{split[-2]}/{split[-1]}"
            index_actions.append(
                {
                    "_index": index_name,
                    "_id": f"{uri}",
                    "_source": {
                        "uri": uri,
                        "label": get_value("label", row),
                        "key": key,
                    },
                }
            )
            docs_count += 1
            if docs_count == bulk_size:
                send_actions = index_actions
                docs_count = 0
                index_actions = []
                yield send_actions

        yield index_actions

    return index_actions


def get_species(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Processing species response...")

    species = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        species.update({row["feature_id"]["value"]: row["species_name"]["value"]})

    f.close()

    logging.info(f"Creation species dictionary from sparql response completed.")

    return species


def get_species_classification(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Processing species classification response...")

    species = {}
    classification = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        species.update({row["feature_id"]["value"]: row["species_name"]["value"]})
        if row.get("classification"):
            classification.update(
                {row["feature_id"]["value"]: row["classification"]["value"].split("|")}
            )

    f.close()

    logging.info(f"Creation species dictionary from sparql response completed.")

    return species, classification


def get_taxon(sparql_response):
    """
    :param sparql_response:
    :return:
    """

    logging.info(f"Processing taxon response...")

    taxon = {}

    f = open(sparql_response, "rb")
    docus = ijson.items(f, "results.bindings.item")

    for row in docus:
        taxon.update({row["taxon_taxon_id"]["value"]: get_taxon_value(row)})

    f.close()

    logging.info(f"Creation species dictionary from sparql response completed.")

    return taxon


def strtobool(val):
    """Convert a string representation of truth to true (1) or false (0).
    True values are 'y', 'yes', 't', 'true', 'on', and '1'; false values
    are 'n', 'no', 'f', 'false', 'off', and '0'.  Raises ValueError if
    'val' is anything else.
    """
    val = val.lower()
    if val in ("y", "yes", "t", "true", "on", "1"):
        return True
    elif val in ("n", "no", "f", "false", "off", "0"):
        return False
    else:
        raise ValueError("invalid truth value %r" % (val,))
