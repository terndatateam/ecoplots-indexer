mapping_settings = {
    "analysis": {
        "analyzer": {
            "tags_analyzer": {"tokenizer": "tags_tokenizer", "filter": ["lowercase"]},
            "spp_class_analyzer": {"tokenizer": "spp_class_tokenizer", "filter": ["lowercase"]},
        },
        "tokenizer": {
            "tags_tokenizer": {"type": "char_group", "tokenize_on_chars": ["whitespace", ","]},
            "spp_class_tokenizer": {"type": "char_group", "tokenize_on_chars": ["|"]},
        },
        "normalizer": {"lowercase_normalizer": {"type": "custom", "filter": ["lowercase"]}},
    }
}

value_label_mapping = {
    "properties": {
        "label": {
            "type": "text",
            "fields": {"keyword": {"type": "keyword", "ignore_above": 256}},
        },
        "value": {"type": "keyword"},
    }
}
text_keyword_mapping = {
    "type": "text",
    "fields": {"keyword": {"type": "keyword", "normalizer": "lowercase_normalizer"}},
}
text_mapping = {"type": "text"}
tags_mapping = {"type": "text", "analyzer": "tags_analyzer"}
spp_class_mapping = {"type": "text", "analyzer": "spp_class_analyzer"}
keyword_mapping = {"type": "keyword"}
keyword_lowercase_mapping = {"type": "keyword", "normalizer": "lowercase_normalizer"}
date_mapping = {
    "properties": {
        "value": {
            "type": "date",
            "format": "yyyy-MM-dd'T'HH:mm:ss'Z'||yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||dd/MM/yyyy||d/MM/yyyy||dd/M/yyyy||d/M/yyyy||epoch_millis",
            "ignore_malformed": False,
        },
        "type": {
            "type": "text",
        },
    }
}
coordinate_mapping = {
    "type": "float",
    "ignore_malformed": True,
    "coerce": True,
    "doc_values": True,
}
integer_mapping = {
    "type": "integer",
    "doc_values": False,
}
integer_doc_values_mapping = {
    "type": "integer",
}
flattened_mapping = {
    "type": "flattened",
}

site_hierarchy_mapping = {
    "properties": {
        "depth": integer_mapping,
        "site_id": keyword_mapping,
        "site_id_label": keyword_mapping,
        "site_type": keyword_mapping,
        "parent_site_id": keyword_mapping,
        "parent_site_id_label": keyword_mapping,
        "parent_site_type": keyword_mapping,
    },
}

generic_attribute_mapping = {
    "type": "nested",
    "properties": {
        "id": keyword_mapping,
        "attribute": keyword_mapping,
        "value": {
            "properties": {
                "label": {
                    "type": "text",
                    "fields": {"keyword": {"type": "keyword", "ignore_above": 256}},
                },
                "value": {
                    "type": "text",
                    "fields": {
                        "float": {
                            "type": "float",
                            "ignore_malformed": True,
                            "coerce": True,
                        },
                        "integer": {
                            "type": "integer",
                            "ignore_malformed": True,
                            "coerce": True,
                        },
                        "boolean": {"type": "keyword", "normalizer": "lowercase_normalizer"},
                        "keyword": {"type": "keyword", "normalizer": "lowercase_normalizer"},
                        "date": {
                            "type": "date",
                            "format": "yyyy-MM-dd'T'HH:mm:ss'Z'||yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||dd/MM/yyyy||d/MM/yyyy||dd/M/yyyy||d/M/yyyy||epoch_millis",
                            "ignore_malformed": "true",
                        },
                    },
                },
                "type": keyword_mapping,
            }
        },
        "unit_of_measure": keyword_mapping,
    },
}

dynamic_templates_site = [
    {"region_as_keyword": {"match": "region:*", "mapping": {"type": "keyword"}}},
    {
        "attribute_field": {
            "path_match": "*_attr_*.attribute",
            "mapping": {"type": "keyword"},
        }
    },
    {
        "id_field": {
            "path_match": "*_attr_*.id",
            "mapping": {"type": "keyword"},
        }
    },
    {
        "unit_field": {
            "path_match": "*_attr_*.unit_of_measure",
            "mapping": {"type": "keyword"},
        }
    },
    {
        "value_label_field": {
            "path_match": "*_attr_*.value.label",
            "mapping": {
                "type": "text",
                "fields": {"keyword": {"type": "keyword", "ignore_above": 256}},
            },
        }
    },
    {
        "value_type_field": {
            "path_match": "*_attr_*.value.type",
            "mapping": {"type": "keyword"},
        }
    },
    {
        "value_value_field": {
            "path_match": "*_attr_*.value.value_float",
            "mapping": {
                "type": "float",
                "ignore_malformed": True,
                "coerce": True,
            },
        }
    },
    {
        "value_value_field": {
            "path_match": "*_attr_*.value.value_int",
            "mapping": {
                "type": "integer",
                "ignore_malformed": True,
                "coerce": True,
            },
        }
    },
    {
        "value_value_field": {
            "path_match": "*_attr_*.value.value_bool",
            "mapping": {"type": "keyword", "normalizer": "lowercase_normalizer"},
        }
    },
    {
        "value_value_field": {
            "path_match": "*_attr_*.value.value_datetime",
            "mapping": {
                "type": "date",
                "format": "yyyy-MM-dd'T'HH:mm:ss'Z'||yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||dd/MM/yyyy||d/MM/yyyy||dd/M/yyyy||d/M/yyyy||epoch_millis",
                "ignore_malformed": "true",
            },
        }
    },
    {
        "value_value_field": {
            "path_match": "*_attr_*.value.value_date",
            "mapping": {
                "type": "date",
                "format": "yyyy-MM-dd'T'HH:mm:ss'Z'||yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||dd/MM/yyyy||d/MM/yyyy||dd/M/yyyy||d/M/yyyy||epoch_millis",
                "ignore_malformed": "true",
            },
        }
    },
    {
        "value_value_field": {
            "path_match": "*_attr_*.value.value_uri",
            "mapping": {"type": "keyword"},
        }
    },
    {
        "value_value_field": {
            "path_match": "*_attr_*.value.value_string",
            "mapping": {"type": "keyword"},
        }
    },
]

dataset_mapping = {
    "mappings": {
        "dynamic_templates": dynamic_templates_site,
        "properties": {
            "dataset_ecoplots_type": keyword_mapping,
            "dataset_class": keyword_mapping,
            "dataset_uri": keyword_mapping,
            "dataset_label": text_mapping,
            "dataset_alt_label": text_mapping,
            "dataset_description": text_mapping,
            "dataset_abstract": text_mapping,
            "dateset_version": keyword_mapping,
            # "dataset_creator": {
            #     "properties": {
            #         "creator": keyword_mapping,
            #         "creator_label": text_mapping,
            #     },
            # },
            # "dataset_creator_label": text_mapping,
            "dataset_created": keyword_mapping,
            "dataset_metadata": keyword_mapping,
            "dataset_publisher": keyword_mapping,
            "dataset_publisher_label": text_mapping,
            "dataset_author": {
                "properties": {
                    "by_dataset": {
                        "properties": {
                            "order": keyword_mapping,
                            "id": keyword_mapping,
                            "name": text_mapping,
                        }
                    },
                    "by_syte": {
                        "properties": {
                            "order": keyword_mapping,
                            "id": keyword_mapping,
                            "name": text_mapping,
                        }
                    },
                },
            },
            "dataset_coauthor": {
                "properties": {
                    "by_dataset": {
                        "properties": {
                            "order": keyword_mapping,
                            "id": keyword_mapping,
                            "name": text_mapping,
                        }
                    },
                    # "by_syte": {
                    #     "properties": {
                    #         "order": keyword_mapping,
                    #         "id": keyword_mapping,
                    #         "name": text_mapping,
                    #     }
                    # },
                },
            },
            "dataset_metadata": {
                "properties": {
                    "by_dataset": keyword_mapping,
                },
            },
            # "dataset_citation": text_mapping,
            "dataset_parent": keyword_mapping,
            "dataset_parent_label": text_mapping,
            "dataset_attributes": keyword_mapping,
        },
    }
}


site_mapping = {
    "settings": mapping_settings,
    "mappings": {
        "dynamic_templates": dynamic_templates_site,
        "properties": {
            "id": keyword_mapping,
            "site_class": keyword_mapping,
            "visit_class": keyword_mapping,
            "site_visit_count": integer_doc_values_mapping,
            "dataset": keyword_mapping,
            "project": keyword_mapping,
            "site_id": keyword_mapping,
            "site_id_label": keyword_lowercase_mapping,
            "parent_site_id": keyword_mapping,
            "parent_site_label": keyword_lowercase_mapping,
            "top_parent_site_id": keyword_mapping,
            "top_parent_site_label": keyword_lowercase_mapping,
            "sites_hierarchy": site_hierarchy_mapping,
            "date_commissioned": date_mapping,
            "geopoint": {"type": "geo_point", "ignore_malformed": True},
            "latitude": coordinate_mapping,
            "longitude": coordinate_mapping,
            "altitude": coordinate_mapping,
            "elevation": coordinate_mapping,
            "dimension": keyword_mapping,
            "site_description": tags_mapping,
            "location_description": tags_mapping,
            "feature_type": keyword_mapping,
            "wkt_point": text_mapping,
            "point_type": keyword_mapping,
            "wkt_polygon": text_mapping,
            "site_attributes": keyword_mapping,
            "site_visit_id": keyword_mapping,
            "site_visit_id_label": text_mapping,
            "visit_start_date": date_mapping,
            "visit_end_date": date_mapping,
            "site_visit_description": tags_mapping,
            "site_visit_location_description": tags_mapping,
            "site_visit_attributes": keyword_mapping,
            "region_types": keyword_mapping,
            "site_visits": {
                "properties": {
                    "site_visit_id": keyword_mapping,
                    "visit_start_date": date_mapping,
                    "visit_end_date": date_mapping,
                },
            },
            "site_citation": keyword_mapping,
            "tags": tags_mapping,
            "scientific_name": text_keyword_mapping,
            "species_classification": text_keyword_mapping,
            "feature_types": keyword_mapping,
            "observed_property": keyword_mapping,
            "used_procedure": keyword_mapping,
        },
    },
}

observation_mapping = {
    "settings": mapping_settings,
    "mappings": {
        "dynamic_templates": dynamic_templates_site,
        "properties": {
            "id": keyword_mapping,
            "observation_class": keyword_mapping,
            "dataset": keyword_mapping,
            "dataset_attr_count": integer_mapping,
            "project": keyword_mapping,
            "project_attr_count": integer_mapping,
            # "parent_site_id": keyword_mapping,
            # "parent_site_id_label": keyword_mapping,
            "site_id": keyword_mapping,
            "site_id_label": keyword_lowercase_mapping,
            # "plot_id": keyword_mapping,
            # "plot_id_label": keyword_mapping,
            # "transect_id": keyword_mapping,
            # "transect_id_label": keyword_mapping,
            # "quadrat_id": keyword_mapping,
            # "quadrat_id_label": keyword_mapping,
            "direct_site_id": keyword_mapping,
            "top_parent_site_id": keyword_mapping,
            "site_attr_count": integer_mapping,
            "sites_hierarchy": flattened_mapping,
            "site_visit_id": keyword_mapping,
            "site_visit_id_label": keyword_lowercase_mapping,
            "site_visit_attr_count": integer_mapping,
            "site_visit_date": date_mapping,
            "site_visit_count": integer_doc_values_mapping,
            "geopoint": {"type": "geo_point", "ignore_malformed": True},
            "latitude": coordinate_mapping,
            "longitude": coordinate_mapping,
            "altitude": coordinate_mapping,
            "elevation": coordinate_mapping,
            "wkt_point": text_mapping,
            "feature_id": keyword_mapping,
            "parent_feature_id": keyword_mapping,
            "feature_class": keyword_mapping,
            "feature_type": keyword_mapping,
            "material_sample_id": keyword_mapping,
            "sampling_id": keyword_mapping,
            "sampling_procedure": keyword_mapping,
            "observed_property": keyword_mapping,
            "used_procedure": keyword_mapping,
            "system_type": keyword_mapping,
            "system_class": keyword_mapping,
            "result_value": {
                "properties": {
                    "label": {
                        "type": "text",
                        "fields": {"keyword": {"type": "keyword", "ignore_above": 256}},
                    },
                    "value": {
                        "type": "text",
                        "analyzer": "tags_analyzer",
                        "fields": {
                            "float": {
                                "type": "float",
                                "ignore_malformed": True,
                                "coerce": True,
                            },
                            "integer": {
                                "type": "integer",
                                "ignore_malformed": True,
                                "coerce": True,
                            },
                            "boolean": {"type": "keyword", "normalizer": "lowercase_normalizer"},
                            "keyword": {"type": "keyword", "normalizer": "lowercase_normalizer"},
                            "date": {
                                "type": "date",
                                "format": "yyyy-MM-dd'T'HH:mm:ss'Z'||yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||dd/MM/yyyy||d/MM/yyyy||dd/M/yyyy||d/M/yyyy||epoch_millis",
                                "ignore_malformed": "true",
                            },
                        },
                    },
                    "taxon_value": {
                        "properties": {
                            "taxon_accepted_name_label": text_keyword_mapping,
                            "taxon_accepted_name": keyword_mapping,
                            "taxon_higher_classification": spp_class_mapping,
                            "taxon_rank": keyword_mapping,
                            "taxon_class": text_keyword_mapping,
                            "taxon_family": text_keyword_mapping,
                            "taxon_genus": text_keyword_mapping,
                            "taxon_kingdom": text_keyword_mapping,
                            "taxon_order": text_keyword_mapping,
                            "taxon_phylum": text_keyword_mapping,
                            "taxon_vernacular_name": text_keyword_mapping,
                            "taxon_name_according_to_label": text_keyword_mapping,
                            "taxon_name_according_to": keyword_mapping,
                            "taxon_nomenclatural_code": keyword_mapping,
                            "taxon_nomenclatural_status": keyword_mapping,
                            "taxon_scientific_name_label": text_keyword_mapping,
                            "taxon_scientific_name_authorship": keyword_mapping,
                            "taxon_concept_id": keyword_mapping,
                            "taxon_taxon_id": keyword_mapping,
                            "taxon_taxonomic_status": keyword_mapping,
                        }
                    },
                    "type": keyword_mapping,
                }
            },
            "result_time": date_mapping,
            "unit_of_measure": keyword_mapping,
            "foi_attributes": keyword_mapping,
            "obs_attributes": keyword_mapping,
            "sampling_attributes": keyword_mapping,
            "system_id": keyword_mapping,
            "system_attributes": keyword_mapping,
            "region_types": keyword_mapping,
            "transect_direction": keyword_mapping,
            # "site_search_fields": tags_mapping,
            "tags": tags_mapping,
            "scientific_name": text_keyword_mapping,
            "accepted_name": text_keyword_mapping,
            "species_classification": text_keyword_mapping,
        },
    },
}

label_mapping = {
    "settings": mapping_settings,
    "mappings": {
        "properties": {"key": keyword_mapping, "uri": keyword_mapping, "label": keyword_mapping}
    },
}
