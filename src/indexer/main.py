import logging
import os

import click

from indexer.es.elastic import (
    get_attributes,
    get_dataset_project_tree,
    get_ufoi,
    get_visit_count,
    get_site_or_visit_tree,
    get_site_or_visit_fts,
    index_attributes_information,
    index_parameters_information,
    index_data,
    index_dataset_information,
    index_site_data,
)
from indexer.es.query import get_query


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


@click.command()
@click.option(
    "--index_alias",
    type=str,
    required=True,
    help="Index alias where create new ES index in.",
)
@click.option(
    "--dataset",
    type=str,
    required=True,
    help="Dataset to index in ES.",
)
@click.option(
    "--temp_dir",
    type=str,
    required=False,
    help="Temp directory where storing the temporary json files. ",
    default=os.getcwd(),
)
def indexer_click(index_alias: str, dataset: str, temp_dir: str):
    indexer(index_alias, dataset, temp_dir)


def indexer(
    index_alias: str,
    dataset: str,
    temp_dir: str = None,
    anzo_connection_url: str = None,
    anzo_connection_user: str = None,
    anzo_connection_pass: str = None,
    elastic_connection_url: str = None,
    elastic_connection_user: str = None,
    elastic_connection_pass: str = None,
):
    logger.info("Starting the indexing")

    anzo_connection_url = (
        anzo_connection_url
        if anzo_connection_url
        else os.environ.get("TRIPLESTORE_URL", "https://anzograph.tern.org.au/sparql")
    )
    anzo_connection_user = (
        anzo_connection_user if anzo_connection_user else os.environ.get("TRIPLESTORE_USERNAME")
    )
    anzo_connection_pass = (
        anzo_connection_pass if anzo_connection_pass else os.environ.get("TRIPLESTORE_PASSWORD")
    )
    elastic_connection_url = (
        elastic_connection_url
        if elastic_connection_url
        else os.environ.get("ELASTICSEARCH_URL", "https://es-test.tern.org.au")
    )
    elastic_connection_user = (
        elastic_connection_user
        if elastic_connection_user
        else os.environ.get("ELASTICSEARCH_USERNAME")
    )
    elastic_connection_pass = (
        elastic_connection_pass
        if elastic_connection_pass
        else os.environ.get("ELASTICSEARCH_PASSWORD")
    )

    try:
        dataset_project_tree = get_dataset_project_tree(
            dataset,
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            temp_dir,
        )

        attributes_dataset_dict = get_attributes(
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            get_query("dataset_attributes", dataset),
            "dataset_id",
            temp_dir,
        )

        index_dataset_information(
            dataset,
            anzo_connection_url,
            get_query("datasets", dataset),
            anzo_connection_user,
            anzo_connection_pass,
            f"{index_alias}-datasets",
            elastic_connection_url,
            elastic_connection_user,
            elastic_connection_pass,
            dataset_project_tree,
            attributes_dataset_dict,
            temp_dir,
        )

        index_attributes_information(
            dataset,
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            f"{index_alias}-attributes",
            elastic_connection_url,
            elastic_connection_user,
            elastic_connection_pass,
        )

        index_parameters_information(
            dataset,
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            f"{index_alias}-parameters",
            elastic_connection_url,
            elastic_connection_user,
            elastic_connection_pass,
        )

        regions_dict = get_ufoi(
            dataset,
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            temp_dir,
        )

        attributes_site_dict = get_attributes(
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            get_query("site_attributes", dataset),
            "site_id",
            temp_dir,
        )

        attributes_site_visit_dict = get_attributes(
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            get_query("site_visit_attributes", dataset),
            "site_visit_id",
            temp_dir,
        )

        site_visit_count = get_visit_count(
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            get_query("visit_count", dataset),
            temp_dir,
        )

        sites_tree_dict = get_site_or_visit_tree(
            "site",
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            get_query("sites_tree", dataset),
            temp_dir,
        )
        # Site fields to be used in Full Text Search
        sites_fts_dict = get_site_or_visit_fts(
            "site",
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            get_query("sites_fts", dataset),
            temp_dir,
        )

        index_site_data(
            dataset,
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            f"{index_alias}-sites",
            elastic_connection_url,
            elastic_connection_user,
            elastic_connection_pass,
            temp_dir=temp_dir,
            regions=regions_dict,
            attributes_site_dict=attributes_site_dict,
            attributes_site_visit_dict=attributes_site_visit_dict,
            dataset_project_tree=dataset_project_tree,
            attributes_dataset_dict=attributes_dataset_dict,
            site_visit_count=site_visit_count,
            sites_fts_dict=sites_fts_dict,
            sites_tree_dict=sites_tree_dict,
            chunk_size=100,
        )

        index_data(
            dataset,
            anzo_connection_url,
            anzo_connection_user,
            anzo_connection_pass,
            index_alias,
            elastic_connection_url,
            elastic_connection_user,
            elastic_connection_pass,
            temp_dir=temp_dir,
            regions=regions_dict,
            attributes_site_dict=attributes_site_dict,
            attributes_site_visit_dict=attributes_site_visit_dict,
            dataset_project_tree=dataset_project_tree,
            attributes_dataset_dict=attributes_dataset_dict,
            site_visit_count=site_visit_count,
            sites_fts_dict=sites_fts_dict,
            sites_tree_dict=sites_tree_dict,
            index_labels=True,
            chunk_size=1000,
        )

        with open("INDEX_SUCCESS", "w") as fp:
            pass

    except Exception as e:
        with open("INDEX_FAILURE", "w") as fp:
            pass
        raise e


if __name__ == "__main__":
    # indexer_click()
    # indexer_click(["--index_alias", "plotdata_ecoplots", "--dataset", "authors2"])
    # indexer_click(["--index_alias", "plotdata_ecoplots", "--dataset", "ausplots-forest"])
    indexer_click(["--index_alias", "plotdata_ecoplots", "--dataset", "tern-surveillance"])
    # indexer_click(["--index_alias", "plotdata_ecoplots", "--dataset", "qbeis"])
    # indexer_click(["--index_alias", "plotdata_ecoplots", "--dataset", "tern-ecosystem-processes"])
    # indexer_click(["--index_alias", "plotdata_ecoplots", "--dataset", "three-parks-savanna"])
    # indexer_click(["--index_alias", "plotdata_ecoplots", "--dataset", "nsw-fmip"])
    # indexer_click(
    #     ["--index_alias", "plotdata_ecoplots", "--dataset", "tropical_rainforest"]
    # )

    # indexer_click(
    #     [
    #         "--index_alias",
    #         "plotdata_ecoplots",
    #         "--dataset",
    #         "bdbsa",
    #     ]
    # )

    # indexer_click(
    #     [
    #         "--index_alias",
    #         "plotdata_ecoplots",
    #         "--dataset",
    #         "wet-tropics-vertebrate",
    #     ]
    # )
