import json
import logging
import os

from elasticsearch import Elasticsearch
import elasticsearch
from elasticsearch_dsl import A, Q, Search
import sqlalchemy
from sqlalchemy.orm import sessionmaker


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

VOCABS_TO_FETCH = [
    "region_type",
    "region",
    "dataset",
    "site_id",
    "site_visit_id",
    "feature_type",
    "observed_property",
    "attributes",
    "units",
    "used_procedure",
    "system_type",
    "core_attributes",
]

ECOPLOTS_INDEX_LABELS = "plotdata_ecoplots-labels"
ECOPLOTS_INDEX_DATA = "plotdata_ecoplots-data"

TERN_CV_PREFIX = "http://linked.data.gov.au/def/tern-cv/"
TERN_CV_SHORT = "tern:"
REGION_TYPE_PREFIX = "http://linked.data.gov.au/dataset/"
REGION_TYPE_SHORT = "region:"


def vocab_fetch(es_client, index):
    search = Search()
    result = {}

    for facet in VOCABS_TO_FETCH:
        scroll_id = None
        returned_docs = 9999
        while returned_docs > 0:
            (
                _,
                _,
                _,
                returned_docs,
                scroll_id,
                _,
                _,
                data,
            ) = _get_data(es_client, f"{index}-{facet}", search.to_dict(), scroll_id)
            for hit in data["hits"]["hits"]:
                result.update({hit["_source"]["uri"]: hit["_source"]["label"]})

        es_client.clear_scroll(scroll_id=scroll_id)

    return result


def get_es_connection(
    es_hosts: str or list, es_user: str, es_pass: str, es_timeout: int = 600
) -> Elasticsearch:
    logger = logging.getLogger(__name__)
    # Build and test connection to Elasticsearch
    es = Elasticsearch(
        hosts=es_hosts,
        http_auth=(es_user, es_pass),
        use_ssl=False,
        verify_certs=False,
        timeout=es_timeout,
    )
    try:
        es.info()
    except Exception as e:
        logger.error("Can't connect to elasticsearch: %s", e)
        raise
    return es


def create_session_factory(connection_string):
    """Creates a thread-safe session factory
    See https://docs.sqlalchemy.org/en/13/orm/contextual.html#contextual-thread-local-sessions
    """
    engine = sqlalchemy.create_engine(connection_string, pool_pre_ping=True)
    Session = sessionmaker(bind=engine)
    return Session


def indexer():
    logger.info("Starting the script")

    db_connection = os.environ.get(
        "DB_CONNECTION", "postgresql://ecoplots_api:tzAjxDgvT3XDn5cCWaem8AVpfPNDw7Gv@localhost:8888/ecoplots_api"
    )

    elastic_connection_url = os.environ.get("ELASTICSEARCH_URL", "https://es-test.tern.org.au")

    elastic_connection_user = os.environ.get("ELASTICSEARCH_USERNAME")

    elastic_connection_pass = os.environ.get("ELASTICSEARCH_PASSWORD")

    es_client = get_es_connection(
        elastic_connection_url, elastic_connection_user, elastic_connection_pass
    )

    Session = create_session_factory(db_connection)
    db_session = Session()
    packages = get_packages(db_session)

    labels = vocab_fetch(es_client, ECOPLOTS_INDEX_LABELS)

    for pkg in packages:
        es_query_labels = get_labels(pkg["es_query"], labels)
        pkg["es_query_labels"] = es_query_labels
        pkg["region_labels_in_data"] = es_query_labels.get("region", [])
        pkg["region_type_labels_in_data"] = es_query_labels.get("region_type", [])

        # Get aggregation of datasets, for citation and usageTracking purposes.
        response = es_client.search(
            index=ECOPLOTS_INDEX_DATA,
            body=query_distinct_column(pkg["es_query"], "region:asgs2016/stateorterritory"),
        )
        _, states_labels_aux = transform_agg_labels(response["aggregations"], labels)
        pkg["states_labels_in_data"] = states_labels_aux

        # Get aggregation of datasets, for citation and usageTracking purposes.
        response = es_client.search(
            index=ECOPLOTS_INDEX_DATA,
            body=query_distinct_column(pkg["es_query"], "dataset"),
        )

        pkg["datasets_labels_in_data"] = get_labels_list(pkg["datasets_in_data"] or [], labels)

        update_datasets_in_data(
            db_session,
            pkg["id"],
            pkg["datasets_labels_in_data"],
            pkg["states_labels_in_data"],
            pkg["region_labels_in_data"],
            pkg["region_type_labels_in_data"],
            pkg["es_query_labels"],
        )


def get_labels(es_query, labels):
    labelled = {}
    for facet, q in es_query.items():
        new_q = []
        for uri in q:
            if labels.get(uri):
                new_q.append(labels.get(uri))
            else:
                new_q.append(uri)
        labelled.update({facet: new_q})
    return labelled


def get_labels_list(list, labels):
    new_q = []
    for uri in list:
        if labels.get(uri):
            new_q.append(labels.get(uri))
        else:
            new_q.append(uri)
    return new_q


def transform_agg_labels(response, labels):
    aggregation = []
    aggregation_labels = []
    bucket = response["value"]["buckets"]
    for agg in bucket:
        aggregation.append(agg["key"])
        if labels.get(agg["key"]):
            aggregation_labels.append(labels.get(agg["key"]))

    return aggregation, aggregation_labels


def query_distinct_column(es_query: dict, column_name: str, size: int = 200) -> dict:
    logger = logging.getLogger(__name__)
    search = Search().extra(size=0)

    region_type = es_query.get("region_type", None)
    search_logic = es_query.get("search_logic", ["or"])

    if es_query.get("revisited", [False])[0]:
        search = search.filter(
            "range",
            site_visit_count={"gt": 1},
        )

    for facet_name, facet_values in es_query.items():
        if facet_name == "region_type" or facet_name == "search_logic" or facet_name == "revisited":
            continue
        if facet_name == "date_from":
            data_range = {"time_zone": "+10:00"}
            data_range.update({"gte": "{}||/d".format(facet_values)})
            search = search.filter(
                "range",
                result_time__value=data_range,
            )
        elif facet_name == "date_to":
            data_range = {"time_zone": "+10:00"}
            data_range.update({"lte": "{}||/d".format(facet_values)})
            search = search.filter(
                "range",
                result_time__value=data_range,
            )
        elif facet_name == "project" and (
            "dataset" not in es_query.keys()
            or (
                "http://linked.data.gov.au/dataset/bdbsa" not in es_query.get("dataset", [])
                and "http://linked.data.gov.au/dataset/tern-ecosystem-processes"
                not in es_query.get("dataset", [])
            )
        ):
            continue
        elif facet_name == "top_parent_site_id" and (
            "dataset" not in es_query.keys()
            or (
                # "http://linked.data.gov.au/dataset/ausplots" not in query.get("dataset", []) and
                "http://linked.data.gov.au/dataset/tern-ecosystem-processes"
                not in es_query.get("dataset", [])
            )
        ):
            continue
        elif facet_name == "search_string":
            sub_query = Q(
                "multi_match",
                **{
                    "query": facet_values[0],
                    "fields": ["tags"],
                    "operator": search_logic[0],
                    "type": "best_fields",
                },
            )
            search = search.query("bool", filter=sub_query)
        elif facet_name == "foi_attr_values":
            sub_query = Q(
                "terms",
                **{
                    f"""foi_attr_{es_query.get("foi_attributes")[0].replace(TERN_CV_PREFIX, TERN_CV_SHORT)}.value.value_uri""": facet_values
                },
            )
            search = search.query("bool", filter=sub_query)
        elif facet_name == "obs_attr_values":
            sub_query = Q(
                "terms",
                **{
                    f"""obs_attr_{es_query.get("obs_attributes")[0].replace(TERN_CV_PREFIX, TERN_CV_SHORT)}.value.value_uri""": facet_values
                },
            )
            search = search.query("bool", filter=sub_query)
        elif facet_name == "region":
            sub_query = Q(
                "terms",
                **{region_type[0].replace(REGION_TYPE_PREFIX, REGION_TYPE_SHORT): facet_values},
            )
            search = search.query("bool", filter=sub_query)
        else:
            sub_query = Q("terms", **{f"{facet_name}": facet_values})
            search = search.query("bool", filter=sub_query)

    search.aggs.bucket("value", A("terms", field=f"{column_name}", size=size))

    if logger.isEnabledFor(logging.DEBUG):
        logger.debug(str(search.to_dict()))
    return search.to_dict()


def _get_data(es_client, index, query, scroll_id=None):
    data = None
    error = None
    error_code = None
    total_docs_count = 0
    returned_docs = 0
    more_pages = True
    no_docs_found = False
    new_scroll_id = None
    scroll_size = 2000

    try:
        scroll = "10m"

        # Initial search
        if scroll_id is None:
            data = es_client.search(
                index=index,
                scroll=scroll,
                size=scroll_size,
                body=query,
            )

            total_docs_count = data["hits"]["total"]["value"]

            returned_docs = len(data["hits"]["hits"])
            if returned_docs in [total_docs_count, 0] or returned_docs < scroll_size:
                more_pages = False
            if returned_docs == 0:
                no_docs_found = True
            new_scroll_id = data["_scroll_id"]
        else:
            # Get subsequent documents after the first request
            # using scroll api
            data = es_client.scroll(scroll_id=scroll_id, scroll=scroll)
            returned_docs = len(data["hits"]["hits"])
            total_docs_count = data["hits"]["total"]["value"]

            if returned_docs < scroll_size or returned_docs == 0:
                more_pages = False

            new_scroll_id = data["_scroll_id"]

    except elasticsearch.exceptions.RequestError as err:
        # 400
        error = {
            "error": {
                "error_type": "RequestError",
                "reason": err.info["error"]["reason"],
                "caused_by": err.info["error"]["type"],
                "status": err.status_code,
            }
        }
        error_code = err.status_code

    except elasticsearch.exceptions.NotFoundError as err:
        # 404
        error = {
            "error": {
                "error_type": "NotFoundError",
                "reason": err.info["error"]["reason"],
                "caused_by": err.info["error"]["type"],
                "status": err.status_code,
            }
        }
        error_code = err.status_code

    except elasticsearch.exceptions.ConnectionTimeout as err:
        # ?
        error = {
            "error": {
                "error_type": "ConnectionTimeout",
                "reason": err.error,
                "status": err.status_code,
            }
        }
        error_code = err.status_code
    except RuntimeError as ex:
        error = {"error": "There was an error communicating with ElasticSearch", "details": ex}
        error_code = 500

    return (
        error,
        error_code,
        total_docs_count,
        returned_docs,
        new_scroll_id,
        more_pages,
        no_docs_found,
        data,
    )


def update_datasets_in_data(
    session,
    id,
    datasets_labels_in_data,
    states_labels_in_data,
    region_labels_in_data,
    region_type_labels_in_data,
    es_query_labels,
):
    statement = sqlalchemy.text(
        """
UPDATE package
SET
    datasets_labels_in_data = :datasets_labels_in_data,
    states_labels_in_data = :states_labels_in_data,
    region_labels_in_data = :region_labels_in_data,
    region_type_labels_in_data = :region_type_labels_in_data,
    es_query_labels = :es_query_labels
WHERE id = :id"""
    )
    session.execute(
        statement,
        {
            "id": id,
            "datasets_labels_in_data": json.dumps(datasets_labels_in_data),
            "states_labels_in_data": json.dumps(states_labels_in_data),
            "region_labels_in_data": json.dumps(region_labels_in_data),
            "region_type_labels_in_data": json.dumps(region_type_labels_in_data),
            "es_query_labels": json.dumps(es_query_labels),
        },
    )
    session.commit()


def get_packages(session):
    statement = sqlalchemy.text(
        """ SELECT
                id,
                es_query,
                datasets_in_data
            FROM package
        """
    )
    packages = session.execute(statement).all()

    if not packages:
        return []

    return [
        {"id": package[0], "es_query": package[1], "datasets_in_data": package[2]}
        for package in packages
    ]


if __name__ == "__main__":
    indexer()
