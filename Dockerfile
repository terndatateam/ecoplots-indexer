# start from an official image
ARG ALPINE_VERSION=3.13

# BUILD and install code
FROM alpine:${ALPINE_VERSION} as builder

# Install build dependencies
RUN apk add --no-cache \
    git \
    python3

# Create virtualenv to use for our app
RUN python3 -m venv --system-site-packages /opt/ecoplots_indexer

# Configure virtualenv as default
ENV VIRTUALENV=/opt/ecoplots_indexer \
    PATH=/opt/ecoplots_indexer/bin:${PATH}

# Install and upgrade python deps in virtualenv
RUN pip install --no-cache-dir --upgrade \
    pip \
    setuptools \
    wheel


# COPY source code into container
COPY . /ecoplots_indexer/
# Install pkg
RUN pip install --no-cache-dir /ecoplots_indexer


# BUILD space optimised final image, based on installed code from builder
FROM alpine:${ALPINE_VERSION} as runner

# # create app user and group
RUN addgroup -g 1000 ecoplots_indexer \
    && adduser -h /opt/ecoplots_indexer -g 'my-app' -G ecoplots_indexer -D -H -u 1000 ecoplots_indexer

# Binary pkgs and other pkgs hard or slow to install from source

RUN apk add --no-cache \
    python3

COPY --from=builder /opt/ecoplots_indexer/ /opt/ecoplots_indexer/

# Configure virtualenv as default
ENV VIRTUALENV=/opt/ecoplots_indexer \
    PATH=/opt/ecoplots_indexer/bin:${PATH}

USER 1000

CMD [ "ecoplots-indexer" ]
